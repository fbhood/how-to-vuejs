
## Methods
Click the view the [Repository](https://bitbucket.org/fbhood/how-to-vuejs/src/master/5-Methods/) 
| Click to view the video on [YouTube](https://youtu.be/dESmaEvkZ2I)

So far we have seen how to bind data using the v-bind directive inside our template. In the next chapter we will learn more directives but before diving into that let's quickly talk about how to put our functions. Since we are working in a big object the Vue instance function will take the name of methods, and as you might guessed the Options object has a property called `methods` where we can store our functions like we do for our data. 

Inside our Vue instance let's define a methods that we can call anything we like, remember to use a naming convention that clearly describe your code.

```js

let app = new Vue({
    el: '#app',
    data: {
        firstName: "Fabio",
        lastName: "Pacific" 
    },
    methods: {
        // es6 syntax
        getFullName(){
            return this.firstName + " " + this.lastName;
        }
        // es5 syntax
    /* getFullName: function(){

        } */
    }
});

```
In the code above we created a method inside the methods object. We called it `getFullName`. Inside a method we have access to the this keyword that refers to the object instance, therefore we can use it to access from a method the properties stored in the data object. When we call the method `getFullName` the method will return a single string that contains both the first and the last name. 

Now inside our HTML file we can simply call the method as we did when we needed to access properties in the data object `{{ getFullName() }}` 

```html
<div>{{ getFullName() }}</div>
```
Now that we know how to create a method and where to put it in the Vue instance let's move forward and learn more about directives.
