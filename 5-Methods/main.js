
/* let dataObject = {

} */

// Vue JS goes here
let app = new Vue({
    // options object
    el: '#app',
    data: {
       firstName: "Fabio",
       lastName: "Pacific"
    }, 
    methods: {
        getFullName(){
            return this.firstName + " " + this.lastName;
        },
        // es5 syntax
/*         getFullName: function(){

        }  */
    }
});