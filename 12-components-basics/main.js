/* Vue.component('test-componet', {
    template: `<p>I am a component</p>`
}) */

Vue.component('tweet-message',{
     /* props: ['text','date'], */
     props: {
        text: String,
        date: String
     },
    template: `
    <div>
        <p :class="tweetBoxWrapper">{{text}}</p>
        <p :class="dateClass">{{now}}</p>
    </div>
    `,
    data(){
        return {
            tweetBoxWrapper: "tweet-message",
            dateClass: "tweet-date",
            now: new Date().toLocaleString(),
            message: this.text
        }
    }

})


Vue.component('tweet-section', {
    props: {
        title: String
    },
    template: `
        <section>
            <h2>{{title}}</h2>
            <slot></slot>
        </section>
    `
})

let app = new Vue({
    el: '#app',
    data: {

    }
})