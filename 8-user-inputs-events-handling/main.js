// Vue JS goes here
let app = new Vue({
    // options object
    el: '#app',
    data: {
        name: "John Doe",
        title: "Portfolio",
        projects: [
            { 
                title: "Portfolio", 
                description:" My portfolio",
                likes: 0
            },
            { 
                title: "Twitter clone", 
                description: "twitter clone",
                likes:0
            },

        ]
    }, 
    methods: {
        addLike(project)
        {
            //console.log(project, "like");
            const projectTitle = project.title;
            // check if the current project is not in the local storage
            if(!localStorage.getItem(projectTitle)) {
                // set the item in the storage and increase the likes counter
                project.likes++;
                localStorage.setItem(projectTitle, true);
            }
          
        },
        removeLike(project){ 
            const projectTitle = project.title;
            console.log(project, "dislike");  
            if(project.likes > 0 && Boolean(localStorage.getItem(projectTitle))) {
                project.likes--;
                localStorage.removeItem(projectTitle);
            }
            
        }
    },
    mounted(){
        this.projects.forEach(project => {
            if(localStorage.getItem(project.title) !== null) {
                project.likes = 1; 
            }
        });
    }

});