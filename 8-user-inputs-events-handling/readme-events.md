## How to handle user inputs with events Handling (v-on)
View the [Repository](https://bitbucket.org/fbhood/how-to-vuejs/src/master/8-user-inputs-events-handling/)
 | View the video on [YouTube](https://youtu.be/9_U1eagqOJY)
 
To make our application react to a user input Vue provides a straightforward directive called `v-on`. This is one of the directives that accepts arguments, similar to the v-bind directive.With such directive listen to events triggered by a user is quick and easy. 
The v-on directive lets us run a function that executes a block of code when the user performs an action for instance, clicks on a button, hover on an element or press a specific key on the keyboard.

### Syntax and Events
There are two type of syntax we can use, the long form or the short, both are equivalent so pick the one you prefer. What follows is just a representation of the syntax, I'll explain it in details in a minute.

long syntax: `v-on:EventName='doSomething' `
shot syntax: `@EventName='doSomething'`

There are many events we can listen to, to list a few we have: 
- click
- submit
- mouseover
- mouseenter
- mouseleave
- keyup
- keydown
- keypress

But we can also create custom events. We will see that when we reach the components section.

Let's pick the long form syntax and explain it `v-on:EventName='doSomething`: 

First we have the directive `v-on` then we have an argument that is the event name we want to listen to, for instance `click` after it, the `doSomething` can be any method that we have defined inside the methods object of the Vue instance.

This method is like any other function that we define inside a JavaScript object. 
It can have parameters or not. If it has, we can call the method and pass parameters to it as usual `doSomething(param, param_2, param3)` etc. 

We can have something like that `<div v-on:click="likeProject">Like</div>` and when the user clicks on this element, we will trigger a method and run some code to increase a likes counter inside a project.

Let's first create the html we need for that:
```html
<div class="projects" v-for="(project,index) in projects">
    <h1>{{project.title.toUpperCase()}}</h1>
    <p>Lorem ipsum dolor sit amet.</p>
    <div>Like
        <i class="fas fa-heart fa-lg fa-fw" @click="likeProject(index)">
        </i>
        {{project.likes}}
    </div>
</div>

```
In the code here we first use the v-for directive to loop over the array of projects, note that we use the syntax `(project, index) in projects` because we will need to pass the index to the like method that we defined earlier. 

After that, we output some data onto the page, like the project name in uppercase letters, then the description, and a `div` tag with an icon for the likes (remember to add fontawesome to get the icon). 

On the heart icon we add the directive v-on using the short syntax `@click="likeProject(index)"` between quotes we invoked our `likeProject(index)` method and, we passed to it the index a parameter so we can find the current project the user clicked on. 

Finally we rendered the likes onto the page for the current project using the `{{project.likes}}` syntax.


Now it's time to go in the Vue instance and write our method.
```js

let app = new Vue({
    el:"#app",
    data: {
        projects: [
            {title: "My first project", description: "A simplified Twitter clone", likes: 0},
            {title: "My second project", description: "Projects portfolio with GitHub", likes: 0},
        ]
    },
        methods: {
            likeProject(index){
                const project = this.projects[index]
                project.likes++
                console.log(project.likes)
            }
        
    }
});

```
As we said earlier we needed to define a method to call when the user clicks on a link, so we created the `likeProject` method, it accepts a parameter that will be the index of the element the user clicked on. We can then add a likes property inside our projects array and access it for the current project to increment its value every time the user clicks on our link.



### Access the original event
If for any reason we need to access the original DOM event we could have used the special `$event` variable inside the method like so `doSomething(param1, param2, $event)` on the v-on directive. Let's see an example of that

We need to add the special variable in the method call on our v-on direction like so:
```html
<i class="fas fa-heart fa-lg fa-fw" @click="likeProject(index, $event)">
        </i>
```
Then we can access the original event inside our method like so 
```js
likeProject(index, event){
    console.log(event); // get the original event
    const project = this.projects[index]
    project.likes++
    console.log(project.likes)
}
```


Now that we know how the v-on directive works, let's improve our Likes example and put something more in it. We will use key modifiers in the next example, so let's quickly see what they are and what we can do with them.

### Event Modifiers
With events Vue provides access to a number of Event modifiers. They are divided into 4 main groups. We can add these modifiers to a directive to change the way our event behaves. They are like a postfix and can be chained using the dot notation. 
Below there is a quick reference:

Categories:
- event modifiers
- key modifiers
- system modifiers keys
- mouse buttons modifiers

Event Modifiers:
.stop
.prevent 
.capture
.self
.once
.passive

Key Modifiers:
We can add these modifiers to the @keyup up listener to listen for when these keys are pressed or use them as a combination with the @click event to listen for a click+space for example. `@click.enter="doSomething"`
.enter
.tab
.delete (captures both “Delete” and “Backspace” keys)
.esc
.space
.up
.down
.left
.right

System modifiers:
With this modifiers we can trigger mouse or keyboard event listeners when the corresponding key is pressed.
.ctrl 
.alt
.shift
.meta
.exact (allows control of the exact combination of system modifiers needed to trigger an event)

Mouse buttons modifiers:
These modifiers allows us to trigger a mouse event listener if the corresponding mouse button is clicked. 

.left
.right
.middle

If you want to learn more, read the docs: [https://vuejs.org/v2/guide/events.html#Event-Modifiers]


### Like a project with key modifiers
In the previous example we used the v-on:click directive to trigger a mouse event listener aiming to simulate a like on a project. But the user was able to add as many likes he wanted by clicking on the icon. It the next example we will do things a bit differently.
- First we will prevent the user from adding more than one like to each project,
- Then we will let the user remove a like 
- Finally we will keep the likes onto the page even after we refresh the page.

Let's get started. This time we will user mouse buttons modifiers to listen for the left mouse button click to trigger the add like behavior and to the right mouse button click to trigger a remove behavior.

inside our html file: 
```html
<div id="app">
    
    <!-- Users can like a project with a left click and dislike it with right click -->

        <div class="projects" v-for="project in projects">
            <h1>{{project.title.toUpperCase()}}</h1>
            <p>Lorem ipsum dolor sit amet.</p>
            <div>Like 
                <i class="fas fa-heart fa-lg fa-fw" 
                    @click.left="addLike(project)" 
                    @click.right="removeLike(project, $event)">
                </i> 
               {{project.likes}}
            </div>
        </div>


</div>
```
In the code above we have taken what we had before and simple added a left mouse button key modifier to the click event `@click.left` then we invoked the `addLike` method. This will make our project likes counter increase of one as we have seen before. Then we have added another event listener to the same element, but this time we used the `.right` mouse button key modifier to listen when the user clicks on our icon using the right button `@click.right="removeLike()"`. In the remove like method we have also passed the special variable $event so that we can use the original event later in our method to prevent its default behavior, open the contextual menu. But we said earlier that key modifiers can also be chained and indeed there is a `.prevent` key modifier that can be used here instead of the `$event` variable. We could do the same like that `@click.right.prevent="removeLike(project)"` 

Let's see how to structure our main.js file:
 
```js
let app = new Vue({
    el: "#app",
    data: {
        name: "John Doe",
        title: "Portfolio",
        projects: [
            {title: "My first project", description: "A simplified Twitter clone", likes: 0},
            {title: "My second project", description: "Projects portfolio with GitHub", likes: 0},
        ]
    },
    methods: {
        addLike(project){
           console.log(project)  
        },
        removeLike(project, event){
            console.log(project)
            console.log(event)
        }
    }

});
```
So in the data object we have a projects property that is array of objects. Each object has a likes property that we will increment or decrement depending on what mouse button the user clicks.
Inside the `methods` object we have created the two methods we referenced in our v-on directives `addLike()` and `removeLike()`. For now we are only logging to the console the project parameter value and the event value. We will implement the logic in a minute.

Let's start with the add likes method, it could look like that:
```js
addLike(project){
    const projectTitle = project.title;
    if(!localStorage.getItem(projectTitle)) {
        project.likes++;
        localStorage.setItem(projectTitle, true);
    }              
}
```
There are a few things going on here, in the first line we store the project title inside the `projectTitle` variable. Then, we said we want data to persist if we refresh the page so we are using the `localStorage` API to store information inside the client browser. We increment the likes count of one but, we do that depending on a value inside the local storage. 
This is done by first checking if there is a key in the `localStorage` matching our project title, `if(!localStorage.getItem(projectTitle))`, if this evaluates to false then we will run the code inside the if block and, first increment the likes `project.likes++`, second use the `.setItem()` method of the local storage API to set a key value pair with the project title as the key and a boolean value as its value `localStorage.setItem(projectTitle, true)` 


To put an item in the local storage we use `localStorage.setItem()` the set item method accepts a key value pair, our key will be the title we saved in the variable `projectTitle` and the value will the a boolean value `true`.

Now let's see it this works. 
Cool, it's time to make the remove feature works.

```js
removeLike(project, event){
    event.preventDefault(); // This can be omitted if we use the prevent key modifier
    const projectTitle = project.title;
    if(project.likes > 0 && localStorage.getItem(projectTitle)) {
        project.likes--;
        localStorage.removeItem(projectTitle);
    }
}
```
This function does the opposite of the previous. When the user clicks with the right button of the mouse the method `removeLikes()` is executed and we do the following:
First thing first, we need to prevent its default behavior or when the user right clicks on the icon the contextual menu will pop up and we don't want that. So, we use the `event.preventDefault()` method on the original event that is represented by the `event` parameter on our method. Alternatively this can be omitted if we use the prevent key modifier in the v-on directive `@click.right.prevent="removeLike(project)`.

The next step is to grab the project title, and since we passed also a parameter to the method to represent the current project object `removeLike(project, event)`, we can store the project title in a variable `projectTitle`.

Then we need to make a couple of checks, first we want to decrement the likes only if its value is greater than zero, then we want to make sure the project title is in the local storage as a key with a value. So, in our condition we have done both checks `if(project.likes > 0 && localStorage.getItem(projectTitle))` now, if both conditions evaluate to true the code inside the if block can run and first we remove the like by decrementing its value `project.likes--` then we remove the project title from the local storage using the `removeItem` method and passing to it the key we want to remove, which is the project title `localStorage.removeItem(projectTitle)`.   

To Put all together we have the following code:

```js
let app = new Vue({
    el: "#app",
    data: {
        name: "John Doe",
        title: "Portfolio",
        projects: [
            {title: "My first project", description: "A simplified Twitter clone", likes: 0},
            {title: "My second project", description: "Projects portfolio with GitHub", likes: 0},
        ]
    },
        methods: {
        addLike(project)
        {
            //console.log(project, "like");
            const projectTitle = project.title;
            // check if the current project is not in the local storage
            if(!localStorage.getItem(projectTitle)) {
                // set the item in the storage and increase the likes counter
                project.likes++;
                localStorage.setItem(projectTitle, true);
            }
          
        },
        removeLike(project){ 
            const projectTitle = project.title;
            console.log(project, "dislike");  
            if(project.likes > 0 && Boolean(localStorage.getItem(projectTitle))) {
                project.likes--;
                localStorage.removeItem(projectTitle);
            }
            
        }
    },
    mounted(){
        this.projects.forEach(project => {
            if(localStorage.getItem(project.title) !== null) {
                project.likes = 1; 
            }
        });
    }

});

```

To make the code work, we also add a life cycle hook called mounted. This will let us run code when the root element is mounted on the vue instance. With it we can check if the localStorage has a key corresponding to our project title and if so, update the value of the likes counter.

And our html is still the same:
```html
<div id="app">
    <!-- Users can like a project with a left click and dislike it with right click -->

    <div class="projects" v-for="project in projects">
        <h1>{{project.title.toUpperCase()}}</h1>
        <p>Lorem ipsum dolor sit amet.</p>
        <div>Like 
            <i class="fas fa-heart fa-lg fa-fw" 
                @click.left="addLike(project)" 
                @click.right="removeLike(project, $event)">
            </i> 
            {{project.likes}}
        </div>
    </div>
</div>
```
Remember that we can get rid of the `$event` variable passed to the `removeLike` method by using the event key modifier like so `@click.right.prevent="removeLike(project)"`


We have learned a lot so far, and now that we have seen key modifiers in action we can move forward to the next topic, two way model biding and the v-model directive and start building our twitter clone. 
