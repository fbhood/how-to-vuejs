
## Two way model binding (v-model)
View the [Repositoy](https://bitbucket.org/fbhood/how-to-vuejs/src/master/9-two-way-binding/) | View the video on [YouTube](https://youtu.be/pBUXTUvDRCo)

### Into
All right, so far we have seen how to bind properties from the data object to our HTML tags and inside attributed, 
how to loop over a sequence of elements and, how to display conditionally elements onto our template with conditionals. 

We have covered how to define methods inside the methods object, so that we can perform more complex operations on our data and, 
we have learned how to work with events using the v-on directive.

In the next section we will look on how Vue opens a two-way communication channel between a form's input and, a property defined
inside the data object. Then we will use this knowledge to build our first project together.

### How does the v-model directive work?
The v-model is another Vue directive. We can use it out of the box, it's useful to simplify the way an input tag can communicate 
with a Vue instance's property in the data object. 

It works like all the other directives, the difference is that when it's implemented our application will listen for changes 
inside the input with this v-model directive and update the attached poperty's value immediately inside the data object and vice-versa. 
It is effectively a two-way comminication channel between our template and the Vue instance. 
It's the Vue way to interact with user inputs and simplify our life as developers.

Let's see a straightforward example.
### How to use the v-model on an input tag
Fist we need an input tag inside the element we defined as the root element in the Vue instance, the div with an id of app. 
```html
<div id="app">
    <h2>What do you want to tweet about today?</h2>
    <input type="text" v-model="tweet" placehoder="What's happening today?">
</div>
```
In the html we have an input tag, it has the v-model directive attached to it as any html attibute. Anything between quotes 
is computed as a javaScript expression, so we write the name of a propety `tweet` that we will create inside the Vue data object.

So let's do it.

```js
let app = new Vue({
    el: '#app',
    data: {
        tweet: ""
    }
});
```
So, now we have a Vue instance and, in it we have a data object with a tweet property that has an empty string as its value. 
If we open the console and inspect the Vue element we can see the two-way data binding in action. 
By changing the value of the tweet property will immetiately update the value inside the input tag and vice-versa. 

Since we have this `tweet` property in the data object and, we already know how to render its content onto the page, 
we can update our markup and add a paragraph under the input tag to see the value dynamically changing while we type.

```html
<div id="app">
    <h2>What do you want to tweet about today?</h2>
    <input type="text" v-model="tweet" placehoder="What's happening today?">
    <p>{{tweet}}<p>
</div>
```
How cool is that? Now we can see the tweet property changing in real time as we type. 
That's the two way data binding. Surely if we change the content of the tweet property directly it will be reflected in our template too.

If you want to lean more make sure to read the official [Documentation](https://vuejs.org/v2/guide/forms.html) too.
### Build a Tweet box 
Now, let's rise the level a little and, build something togheter.

- We will create a simple `textarea` with a submit button, 
- We will display the number of characters the user has left while he is typing 
so that he can submit the form without exceeding the max number of characters allowed. 
- Like in a tweet, the maximum number of characters will be 200.

#### Define the initial makup
Now we need to define a markup so, inside our index.html file we will write the following
```html
<div id="app">
    <h2>What do you want to tweet about today?</h2>
    <form v-on:submit.prevent="submitData">
       <!-- Code here -->

    </form>
    <!-- More code here -->
</div>
```
We have first created our root element for the Vue instance so that it can monitor the markup and do its magic.
Then we created a form tag with an event listener using the directive v-on, it listen for the submit event and runs a function `submitData` that we still have to create, we also added the `.prevent` modifier so that the page doesn't refresh when we submit the form.

#### Define the vue instance and methods
Let's define our vue instance and create the `submitData` method so that we can use it later when we need it.
```js

let app = new Vue({
    el: '#app',
    data: {
        // data object props here
    },
    
    methods: {
          submitData(){
             // Code here
        }
    },

});
```

#### Add a text area and a submit button
Now back to our html let's add the text area inside our form. 

```html
<div id="app">
    <h2>What do you want to tweet about today?</h2>
    <form v-on:submit.prevent="submitData">
       <!-- Code here -->
        <div class="form_group">
            <label for="name">Tweet</label>
            <textarea name="tweet" id="tweet" cols="80" rows="10" v-model="tweet" maxlength="200"></textarea>
            
        </div>

        <button type="submit">Tweet</button>

    </form>
    <!-- More code here -->
</div>

```
Inside the form tag we create a label and a `textarea` for our tweet box. 
On the `textarea` we use the directive v-model to bind the `textarea` value to a `tweet` property and vice-versa. 
So now when when one changes the other changes too. 

NOTE: The v-model directive is used on form elements like inputs, text areas, check box etc.

After the `textarea` we put a button of type submit so that when it's clicked the form's data are sent to our application's `submitData()` method and we can process them.

#### Add properties to the vue instance
Now inside our JavaScript file we need to create the tweet property in the data object and do something with this informations so that we can later show a list of tweets sent. 

We also said that we want to limit the characters to 200 and show an error when in excess.

So let's add a few more properties here like:
- `tweet` for the current tweet message that the user inputs in the text area
- `tweets` for the list of tweets
- `max_length` for the characters limit

```js

let app = new Vue({
    el: '#app',
    data: {
        tweets: [],
        tweet: "",
        max_length: 200,  
    },
    
    methods: {
          submitData(){
              /* Handle the tweet */
        }
    },

});
```

So, now with the tweets property as an array and using the two-day binding between the tweet property and the `textarea` to we can push all tweets messages inside the `tweets` array when the user submits the form by triggering the 
`submitData` method.
#### Implement the characters counter
Before implement the submit data we can show a character counter while the user types in the textarea. 
So, let's implement this feature so the user knows if he can submit the tweet or not.

Back in the HTML file we can add a div with a couple of span elements and use a v-if directive to check the characters length and, show the counter while the user is within the characters limit, otherwise we can show an error message.

```html
<div id="app">
    <h2>What do you want to tweet about today?</h2>
    <form v-on:submit.prevent="submitData">
        <div class="form_group">
            <label for="name">Tweet</label>
            <textarea name="tweet" id="tweet" cols="80" rows="10" v-model="tweet"></textarea>        
        </div>

        <button type="submit">Tweet</button>

    </form>
    <!-- Show character limits here -->
    <div>
        <span v-if="tweet.length < max_length"> {{ `Max: ${tweet.length} of ${max_length} characters` }}
        </span>
        <span class="errorMessage" v-else>{{`Max char limit reached! excess chars: ${max_length - tweet.length}`}}</span>
    </div>
    
</div>

```
What the code above does is use the two-way data binding between the tweet property and the `textarea` to find 
out if the user has reached the characters limit defined as the `max_lenght` property. 
Since the tweet property is connected to the `textarea` we can use the `v-if` directive combined with the `tweet.length` and the `max_length` properties to make the comparison. 

Now, every time the user types something in the `textarea` the string saved in the `tweet` property increases of one character and we can use the `.length` property to see how long the whole string is and, compare it against our `max_length` property.

We used the directive  `v-if="tweet.length <= max_length"` to make our comparison. 
While this comparison returns true, the user will see the span tag with its content, the counter. 
Inside the span tag we used the mustache syntax to show to the user the current length of the property `tweet` and, the characters limit.

```html
<span v-if="tweet.length < max_length"> {{ `Max: ${tweet.length} of ${max_length} characters` }}</span>
```

After the `v-if` directive a `v-else` directive handles the error message shown to the user when there are no characters left to use. 
Here the content of the span element shows a message to inform how many characters in excess there are by subtracting the tweet length to the `max_lenght` property.

```html
<span class="errorMessage" v-else>{{`Max char limit reached! excess chars: ${max_length - tweet.length}`}}</span>
```
#### Submit the form
All there is left is to add the tweet to the list of tweets and show them onto the page when the form is submitted.
Let's complete our `submitData` method so that every time it's executed it pushes a new object to the tweets array. 

Inside our methods object the `submitData` methods now looks like that:
```js
 submitData(){
    if (this.tweet.length <= this.max_length) {
        this.tweets.unshift(this.tweet);
        this.tweet = "";
    } 
}

```
The method above first checks if the length of the `tweet` property is less or equal to the `max_length` property, if the condition evaluates to true, then we can add the `tweet` content to the array using the `unshift` method to add it at the beginning of the array.

Finally, we need to clear the value of the `tweet` property and we do so by assigning to it an empty string again.

NOTE: that since we are inside a method we need to use the `this` keyword to grab properties and eventually methods 
inside the Vue instance.
#### Show a list of tweets
Now, we can also show a list of tweets in our template. 
To do that we will use a `v-for` directive and, loop over the `tweets` array to show each tweet.

```html
<ul>
    <li v-for="text in tweets">{{text}}</li>
</ul>

```

### Put all together
The final code now looks like that

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VueJs v-model</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Fontawesome CDN -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
        integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <!-- VueJS CDN -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
    <style>

    </style>
</head>

<body>
    <div id="app" class="container">

        <h2>What do you wanna tweet about today</h2>

        <!-- Tweet form -->
        <form v-on:submit.prevent="submitData">

            <div class="form-group">
                <label>Tweet</label>
                <textarea class="form-control" cols="30" rows="5" v-model="tweet"></textarea>
            </div>

            <button type="submit" class="btn btn-primary">Tweet</button>
        </form>

        <!-- Alert the user  -->
        <div class="my-3">
            <span v-if="tweet.length < max_length">
                {{ ` Max: ${tweet.length} of ${max_length} characters` }}
            </span>
            <span class="alert alert-danger" v-else> {{ `Max char limit reached! excess characters: ${max_length -
                tweet.length} ` }}</span>

        </div>

        <!-- Tweets message -->
        <ul>

            <li v-for="tweet in tweets">
                {{tweet}}
            </li>
        </ul>

    </div>

    <script src="./main.js"></script>

</body>

</html>

```
Our final javascript file 

```js 

let app = new Vue({
    el: '#app',
    data: {
        tweet: "",
        tweets: [],
        max_length: 200        
    }, 
    methods: {
        submitData(){
            // Handle the tweet submission
            if(this.tweet.length <= this.max_length){
                this.tweets.unshift(this.tweet);
                this.tweet = "";
            }
        }
    }
})


```

### Improvements Todo:
If we take this bit of code from our index.html file there is something we can do to clean up our code... 

```html
<!-- Show the max char messages -->
<div>
    <span v-if="tweet.length < max_length"> {{ `Max: ${tweet.length} of ${max_length} characters` }}
    </span>
    <span class="errorMessage" v-else>{{`Max char limit reached! excess chars: ${max_length - tweet.length}`}}</span>

</div>
```
To clean this template file, we can follow two approaches that are exactly the same except that one is cached 
the other one isn't. 
- computed Properties (cached)
- Methods (not cached)

In the next chapter we will learn what computed properties are and, how they differ from methods.
