## Fundamentals: Installation 
Click to view the [Repository](https://bitbucket.org/fbhood/how-to-vuejs/src/master/1-installation/)

Click to view the [YouTube-Video](https://youtu.be/enz0Vi3NuDA)

We can use Vue in our projects by installing it using a package manager like NPM or using its CDN. If you never used Vuejs before, I suggest you use the CDN, it will be easier if you want to code along with me. 

### CDN
For the CDN we only need to include the script tag below inside our HTML file

```html
<!-- Development version for prototyping and learning -->
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
```
Alternatively, you can use a production-ready script, that uses a specific stable release.

```html
<!-- Production version -->
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
```

In production, Vue suggests using the optimized version to replace vue.js with vue.min.js.


There is also an ES Modules compatible build:
```html
<script type="module">
  import Vue from 'https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.esm.browser.js'
</script>
```
### Install via NPM
To build large scale applications the suggested installation is via NPM

```bash
npm install vue
```

As I said we will use the VueJS CDN to let anyone follow this guide so our final HTML file will look something like that: 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VueJS Tutorial</title>
    
    <!-- vue development version, includes helpful console warnings -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

</head>
<body>



    <script src="./main.js"></script>
</body>
</html>

```
Let's break it down, first, we add a basic markup for an HTML file, then we include the script tag for the VueJs framework. 
In the end, before closing the body tag, we add our main.js script where we place all JavaScript code 
for our application. 
Let's now move to the next step and, add our first Vue instance inside the main.js file.
