# How to build a single page application with Vue.js 
[YouTube-Video](url:https://youtu.be/CzgP6GamIMc)

In this guide, we will build two simple projects and deploy them on Netlify. We will use VueJS as our front-end framework, and use different technologies to build our projects. We will build a kind of Twitter, a very simplified version and a single page application for a portfolio using the GitHub API.

## What is Vue.js
VueJS is a JavaScript framework that has become really popular in recent years. In this guide, we will start looking at the fundamentals first, with a quick look at two libraries VueRouter and Axios that we will use to build a cool portfolio project.

## What do you need to follow this tutorial
TTo follow along, you will need at least a basic knowledge of HTML/CSS and JavaScript. Vue.js knowledge isn't required, we will learn the basics first then, we will move into building our projects together.

This guid has sixteen chapters:
## Chapters
- 1. Fundamentals: Installation
- 2. Fundamentals: Create a Vue Instance
- 3. Fundamentals: Work with templates 
- 4. Fundamentals: Methods
- 5. Fundamentals: Directives Overview
- 6. Fundamentals: Conditionals 
- 7. Fundamentals: Loops
- 8. Fundamentals: How to handle user inputs with events Handling
- 9. Fundamentals: Two way model binding (v-model)
- 10. Fundamentals: Computed Properties and methods
- 11. Project: Simple Twitter Clone
- 12. Fundamentals: Component basics
- 13. Project Update: Simple Twitter clone with components
- 14. Fundamentals: Axios and RestAPI
- 15. Fundamentals: Routing with VueRouter
- 16. Final Project: build a Portfolio with VueJS, VueRouter, Axios, GitHub API and deploy to netlify 

