## Conditionals (v-if/v-else-if/v-else/v-show)
Click to view the [Repository](https://bitbucket.org/fbhood/how-to-vuejs/src/master/6-Conditionals/])
 | Click to view the video on [YouTube](https://youtu.be/VNaCsloA1ZU)
 
It's now time to learn more directives and we will start by looking at how conditionals work in VueJS. The first directive of this section is the `v-if`, it allows us to render blocks of code based on a certain condition. 

Like the `if-else` statements in vanilla JavaScript the v-if will check if the returned value of a conditional expression evaluates to true, and if so it will render the the HTML element and everything we place inside it. 

Since it's a directive it works on a single element (HTML tag) if we want to extend its behavior on multiple elements then we need to wrap them inside a `<template>` tag.

The v-if directive works in the same way the v-bind directive works, it has access to the properties in the data object, and accepts an expression between its quotes. 
If the returned value of the expression or the value of the data property used evaluates to `true` than the directive renders the HTML element otherwise it doesn't.

Obviously, we can check for multiple conditions and end up rendering an element if none of these evaluate to true. We do that using v-if together with the v-else-if and v-else directives.

Let's see a simple example and write some code inside our main.js file to show or hide an element.
The first thing to note is that if we have a property that returns a boolean value, it is enough to use it inside the v-if directive to show/hide an element, like so: 

```html
<h1 v-if="showTitle">{{movieTitle}}</h1>
```
And a vue instance with a showTitle property set to true.
```js

let app = new Vue({
    el: "#app",
    data: {
        movieTitle: 'Shining',
        showTitle: true,
    }
})
```
In such case, we are saying show the title property only if the value of `showTitle` is `true`. If we change it to false the title won't be shown.

Obviously we can put inside the quotes of a v-if directive a simple expression that once computed evaluates to a boolean.
```html
<h2 v-if="age >= 18">{{movieTitle}}</h2>

```
Inside our main.js file
```js 
let app = new Vue({
    el: "#app",
    data: {
        movieTitle: 'Shining',
        age: 18,
    }
})
```
In the code above we wrote an expression on the v-if directive that checks if the `age` property on is greater or equal to 18 if the result is true then the `h2` will be shown onto the page.

Now let's move to a more complex example and add another condition using the v-else-if.

#### v-if/v-else-if
In the following example we will first create a v-if condition similar to the one above but this time we will check if the user is over 18 but under 21 using the `&&` operator. If true we will show the time with an additional note. If instead the user if over 21 then we will simply show the title of the movie. 
```html
<h2 v-if="age > 21">{{movieTitle}}</h2>
<h2 v-else-if="age > 18 && age < 21"> {{ movieTitle }} | Watch with an adult</h2>
```
Inside the Vue instance we could have an `age` property but to make our simple program dynamic we could use a prompt to ask to the user the age. 

```js
let userAge = Number(prompt("What's your age?"))
let app = new Vue({
    el: "#app",
    data: {
        movieTitle: 'Shining',
        age: userAge,
    }
})
```
So the code here first asks to the user what's his/her age, then it stores the result as a number inside the variable `userAge`. The `userAge` variable is later used inside the data object to assign a value to the `age` property so that based on its value we will render an element or the other.

Let's move forward and using the v-else directive show a different message in case the user is under 18.
#### v-else:
The `v-else` directive works differently. We don't have to pass to it anything. It simply enters in action when none of the previous conditions evaluate to a true value.

So the new html element is farily simple:
```html
    <div id="app">
        <h2 v-if="age > 21">{{movieTitle}}</h2>
        <h2 v-else-if="age > 18 && age < 21"> {{ movieTitle }} | Watch with an adult</h2>
        <p v-else> Sorry You are too young to see this movie</p>
    </div>

```
here we have a `p` tag with a v-else directive attached. As you see it looks like an attribute without values, (like the disabled or required html attributes)

Our JavaScript file has not been changed.
```js

let userAge = Number(prompt("What's your age?"))
let app = new Vue({
    el: "#app",
    data: {
        movieTitle: 'Shining',
        age: userAge,
    }
})
```

That's all we need to know about conditional rendering to be able to move forward with our first project but, if you want to learn more here is the documentation: [https://vuejs.org/v2/guide/conditional.html]

We need to learn a few more things before we are able to build our first project, a simplified twitter clone. The next topic is loops.
