## Simple Twitter Clone
Watch the video on [YouTube](https://youtu.be/v1j_bDDd6jI) | View the Repository [here](https://bitbucket.org/fbhood/how-to-vuejs/src/master/11-project-simple-twitter-cone/)

Now, let's put everything we learned so far and, build our very first project, a minimalist and simplified twitter like web application. 

We want to create a simple application that has a kind of registration form, a box to add new tweets and, a section where we can show all tweets. We also want to be able to remove tweets.

All data must be persistent so that after the page is refreshed the list of tweets is still visible while the registration form will be hidden.

### Define Tasks
Let's break this down into large tasks first than we will see what we need to do to complete each one. 
- create a registration form
- create a tweets box form
- create a tweets section

To do our project we need to research on what tools we need to use to achieve our goals, so lets put them down:
- VueJS (application logic)
- localStorage (make data persistent)
- fontawesome (icons)

This application has no database so we are unable to record multiple users and their tweets. 
It's just a proof of concept, something we build to use our new knowledge.
### Create the project structure
Now that we know what to do let's start by creating our project structure and import the tools we need to complete the first task, the registration form.

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Simple tweetter clone</title>
    <!-- CDN Fontawesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
    
    <!-- VueJS development version, includes helpful console warnings -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- Style sheet -->
    <link rel="stylesheet" href="style.css">
</head>

<body>
<div id="app">
    <!-- Register an account -->

    <!-- Add a tweet -->

    <!-- Show all tweets -->
    
</div>
<!-- Link our main.js file -->
<script src="./main.js"></script>
</body>

</html>

```

Now that our HTML file is ready let's create the main.js file and create a Vue instance.

```js

let app = new Vue({
    el: '#app',
    data: {
        
    },
    methods: {
          
    }

});

```

Finally we need to create a style.css file that we will place for now in the root folder of our project. 
We will use a css file that I have already witten, you can download it from [here](https://bitbucket.org/fbhood/simple-tweet-app/src/master/style.css)

OK, our basic structure is ready to go. Inside our HTML file we have some comments that reflect our 3 main tasks, create a registration form, create an add tweet box and, show a list of tweets. 
Let's start with the first task and, simulate a registration form.

### Simulate a registration form - HTML

Inside the root element `<div id="app"></div>` we need to create a registration form with the following fields: name, email, password and a submit button. The form is contained in a card so we will wrap everything in a div and, assign to it a card class. 
The form won't submit data to a server but, simply simulate a registration and, update some property in the data object of the Vue instance.

We will place the following code inside our HTML file
```html
<!-- Register an account -->
<div class="card">
    <i class="fab fa-twitter fa-lg fa-fw"></i>
        
    <h2>Create your account</h2>
    <form v-on:submit.prevent="registerAccount">
        <div class="form_group">
            <label for="name">Name</label>
            <input type="text" v-model="name" maxlength="25" required>
        </div>
        <div class="form_group">
            <label for="email">Email</label>
            <input type="email" v-model="email" maxlength="25" required>
        </div>
        <div class="form_group">
            <label for="password">Password</label>
            <input type="password" v-model="password" maxlength="16" required>
        </div>
        <button type="submit">Register</button>
    </form>

</div>
```
Let's break this down, first the form tag has a `v-on` directive with a `submit` argument so it will listen for a submit event. 
It also has an event modifier `.prevent` so when we hit the submit button the page doesn't refresh. 

Inside the `v-on` directive there is a method call `registerAccount` that we need to create inside the methods of the vue instance.

Inside the form we have the tree input fields: name, email and password with labels. 
We wrapped each field inside a div with a class of `form_group`, later we can replicate the style of the twitter registration fields and, show a characters counter. 

Each input field has a `v-model` directive that binds the input to its data property. 

### Simulate a registration form - VUE
Let's move in the Vue instance to make the binding between the form and the data object properties.

Here we also need a place where store the details that the user submits so we will create another property for that. Looking at the input fields we also put a `maxlength` property on them, which is 25 for the name and email and 16 for the password. 

Like we did previously when we learned about the v-model we can create a property for these limits so that we can use it to show to the user how many characters the user has left. 

The Vue instance will look like that:

```js

let app = new Vue({
    el: '#app',
    data: {
        userData: {}
        name: "",
        email: "",
        password: "",
        max_length: 25,
        max_pass_length: 16
    },
    methods: {
        registerAccount(){
            // record user details
            // add registration to localStorage
            // clear the registration fields

        }
    }

});

```
Let's break this down, first inside the `data:{}` object we defined an object to store and retrive the `userData`, 
then we added properties `name`, `email`, `password` to make the two way data binding work. 
Finally we added the `max_length` and `max_pass_length` propeties.

### Show inputs characters counter 
OK, now that we have a binding between the input fields and, our properties we can show a characters counter to the user while typing. 
This is fairly simple, all there is to do is to show the length of each input property and, compare it with the max length properties we have set in the Vue instance.

```html
<div class="form_group">
    <label for="name">Name
        <span> {{ name.length + '/' + max_length }}</span>
    </label>
    <input type="text" v-model="name" :maxlength="max_length" required>
</div>

```
So here we created a string using an in-template expression and shown the length of the `name` property and the value of the `max_length` so that while the user types we show something like that 13/25. We also used a `v-bind` directive on the `maxlength` attribute so that its value is bound to the value of the property we defined in the Vue instance, in case we want to change it we can do so in one place.

We will do the same for the other fields. 

```html
<form id="register" v-on:submit.prevent="registerAccount">
    <div class="form_group">
        <label for="name">Name
            <span> {{ name.length + '/' + max_length }}</span>
        </label>
        <input type="text" v-model="name" :maxlength="max_length" required>
    </div>
    <div class="form_group">
        <label for="email">Email
            <span> {{ email.length + '/' + max_length }}</span>
        </label>
        <input type="email" v-model="email" :maxlength="max_length" required>
    </div>
    <div class="form_group">
        <label for="password">Password
            <span> {{ password.length + '/' + max_pass_length }}</span>
        </label>
        <input type="password" v-model="password" :maxlength="max_pass_length" required>
    </div>
    <button type="submit">Register</button>

</form>

```

### Add the logic to the registerAccount method
Now it's time to work on the form submission logic. 
What we will do is simply populate the object stored in the property `userData` when the user submits the form. 
Inside the `registerAccount` method we will add the details the user passes and build our object. 

```js
 registerAccount(){
            // record user details
            this.userData.name = this.name,
            this.userData.email = this.email,
            this.userData.password = this.password
            
            // add registration to localStorage

            // clear the registration fields
            this.name = "";
            this.email = "";
            this.password = "";
        }

```
Here we have taken the value of the properties name, email and password and assigned them to properties we created in the `userData` object.
This seems fine mostly because we have put on our input fields the `required` attribute, but if we remove it we will be able to submit an empty form and we don't want that. So let's add a very basic form of validation to at least check if the user has typed something inside our form otherwise we show an error. So we need to add an if block inside the method and also an error property to the data object. Our file now looks like that

```js


let app = new Vue({
    el: '#app',
    data: {
        userData: {},
        usersID: 0,
        name: "",
        email: "",
        password: "",
        max_length: 25,
        max_pass_length: 16,
        error: "",
      
    },  
    methods: {
        registerAccount(){
            if (this.name !== "" && this.email !== "" && this.password !== "" ) 
            {
                this.userData.id = ++this.usersID,
                this.userData.name = this.name,
                this.userData.email = this.email,
                this.userData.password = this.password
                 
            } else {
                this.error = "Complete all the form fields"
            }
        
        /* Add registration data to the local storage */

        
        /* Clear the registration inputs */
        this.name = "";
        this.email = "";
        this.password = "";
        }

    }
    
});

```
Here in the `registerAccount` we wrote a conditional and checked if the length of the name property was not empty and if the email property wasn't empty and if the password wasn't empty  `this.name !== "" && this.email !== "" && this.password !== ""` 
if all these checks evaluate to a true value then we run the code inside the block otherwise we run the codo in the `else` block that updates the value of the `error` property that we will now use in our template to show the error message. 

We also added a new property `usersID: 0,` that we used inside the if block to assign an id property to the `userData` object, just to make our application more realistic but obviously it is useless as we will not have a database where store all users details but only store a single user inside their browser's local storage.

```html
<form id="register" v-on:submit.prevent="registerAccount">
    <div class="form_group">
        <label for="name">Name
            <span> {{ name.length + '/' + max_length }}</span>
        </label>
        <input type="text" v-model="name" :maxlength="max_length" required>
    </div>
    <div class="form_group">
        <label for="email">Email
            <span> {{ email.length + '/' + max_length }}</span>
        </label>
        <input type="email" v-model="email" :maxlength="max_length" required>
    </div>
    <div class="form_group">
        <label for="password">Password
            <span> {{ password.length + '/' + max_pass_length }}</span>
        </label>
        <input type="password" v-model="password" :maxlength="max_pass_length" required>
    </div>
    <button type="submit">Register</button>
</form>


<div v-if="error.length > 0"> {{error}}</div>
```
Now our form is complete and we also display an error message to the user if the required attributes are removed from our markup. 
But our data do not persist and when the page is refreshed it's all gone.
Let's tackle that using the localStorage API.

Inside our vue instance we need to set an item in the local storage but also save the entire `userData` object so that later we can use its data to display a message to the registered user.


```js
/* Add registration data to the local storage */
localStorage.setItem('simple_tweet_registered', true)
/* Add the whole userData object as JSON string */
localStorage.setItem('simple_tweet_registered_user', JSON.stringify(this.userData))

```
Here we use the `setItem` method of the local Storage API to add an item to the local storage so that later we can use it to check if the user is registered or not. 
Then we also need to store the entire `userData` object as a string, to do that we use the `JSON.stringify` method that will turn the object in a json string that can be saved in the localStorage.

Our js file now is as follow
```js

let app = new Vue({
    el: '#app',
    data: {
        userData: {},
        usersID: 0,
        name: "",
        email: "",
        password: "",
        max_length: 25,
        max_pass_length: 16,
        error: "",
    },  
    methods: {
        registerAccount(){
            if (this.name !== ""  && this.email !== "" && this.password !== "" ) {
                this.userData.id = ++this.usersID,
                this.userData.name = this.name,
                this.userData.email = this.email,
                this.userData.password = this.password
                 
            } else {
                this.error = "Complete all the form fields"
            }
        
        /* Add registration data to the local storage */
        localStorage.setItem('simple_tweet_registered', true)
        /* Add the whole userData object as JSON string */
        localStorage.setItem('simple_tweet_registered_user', JSON.stringify(this.userData))

        
        /* Clear the registration inputs */
        this.name = "";
        this.email = "";
        this.password = "";
        }

    }
    
});

```


Now when the user visits our application page we need to check inside the browser local storage if there is a key called `simple_tweet_registered` if there is we can assume that the user is registered and we can show the next section, the tweet box. 
Otherwise we show the registration form. 
We will do that by creating a `registered: false` property in the data object and use it to display or hide the registration form.

```js

data: {
    userData: {},
    usersID: 0,
    name: "",
    email: "",
    password: "",
    max_length: 25,
    max_pass_length: 16,
    error: "",
    registered: false,      
}

```
Wrap the form around a div with a directive `v-if="!registered"`

```html
<div class="register" v-if="!registered">
    // here goes the form
</div>

<div v-else> Tweetbox </div>
```

Our final html file now looks like that:

```html
    <div class="card">
        <i class="fab fa-twitter fa-lg fa-fw"></i>
        <!-- Register an account -->
        <div class="register" v-if="!registered">
            <button form="register" type="submit">Register</button>
            <h2>Create your account</h2>
            <form id="register" v-on:submit.prevent="registerAccount">
                <div class="form_group">
                    <label for="name">Name
                        <span> {{ name.length + '/' + max_length }}</span>
                    </label>
                    <input type="text" v-model="name" :maxlength="max_length" required>
                </div>
                <div class="form_group">
                    <label for="email">Email
                        <span> {{ email.length + '/' + max_length }}</span>
                    </label>
                    <input type="email" v-model="email" :maxlength="max_length" required>
                </div>
                <div class="form_group">
                    <label for="password">Password
                        <span> {{ password.length + '/' + max_pass_length }}</span>
                    </label>
                    <input type="password" v-model="password" :maxlength="max_pass_length" required>
                </div>
            </form>


            <div v-if="error.length > 0"> {{error}}</div>
        </div>
        <!-- Add tweet -->
        <div class="tweetBox" v-else>
            <h2>Welcome username_here write your first Tweet</h2>
        </div>

    </div>

```

Now to make this work we will use the created life cycle hook, that let us inject our code when the vue instance has been created. 
This is because we want to check this before mounting our root element. 

So let's add a created life cycle hook to the Vue instance, here we will check if there is our key and if so we will update the value of the `registered` property to `true`. 

We have also stored the full `userData` object in the local Storage so we can use it to repopulate the `userData` object when the page is refreshed with the details the user submitted.

```js
created(){
    /* Check if the user is registered and set the registered to true */
    if(localStorage.getItem("simple_tweet_registered") === 'true'){
        this.registered = true;
    }
    // repopulate the userData object
     if(localStorage.getItem('simple_tweet_registered_user')) {
            this.userData = JSON.parse(localStorage.getItem('simple_tweet_registered_user'))
        }

}

```
To turn a JSON string back into an object we can use the `JSON.parse` method.

Now it's all set for the next task, show a tweet form to the user after registration.

Our code so far looks like that:

the main.js file
```js

let app = new Vue({
    el: '#app',
    data: {
        userData: {},
        usersID: 0,
        name: "",
        email: "",
        password: "",
        max_length: 25,
        max_pass_length: 16,
        error: "",
        registered: false,
    },
    
    methods: {
          registerAccount(){
      
              if (this.name.length > 0 && this.name.length <= this.max_length && this.email !== "" && this.password !== "" ) {
                  
                    this.userData.id = ++this.usersID,
                    this.userData.name = this.name,
                    this.userData.email = this.email,
                    this.userData.password = this.password
                    this.registered=true;
                
                  
                 
              } else {
                  this.error = "Complete all the form fields"
              }
            
            /* Add registration data to the local storage */
            localStorage.setItem('simple_tweet_registered', true)
            /* Add the whole userData object as JSON string */
            localStorage.setItem('simple_tweet_registered_user', JSON.stringify(this.userData))
            
            /* Clear the registration inputs */
            this.name = "";
            this.email = "";
            this.password = "";
        }
        
    },
    created(){
        /* Check if the user is registered and set the registered to true */
        if(localStorage.getItem("simple_tweet_registered") === 'true'){
            this.registered = true;
        }
        // repopulate the userData object
        if(localStorage.getItem('simple_tweet_registered_user')) {
            this.userData = JSON.parse(localStorage.getItem('simple_tweet_registered_user'))
        }
       
    }

});

```
And the HTML inside the `app` element

```html
<div class="card">
    <i class="fab fa-twitter fa-lg fa-fw"></i>
    <!-- Register an account -->
    <div class="register" v-if="!registered">
        <button form="register" type="submit">Register</button>
        <h2>Create your account</h2>
        <form id="register" v-on:submit.prevent="registerAccount">
            <div class="form_group">
                <label for="name">Name
                    <span> {{ name.length + '/' + max_length }}</span>
                </label>
                <input type="text" v-model="name" :maxlength="max_length" required>
            </div>
            <div class="form_group">
                <label for="email">Email
                    <span> {{ email.length + '/' + max_length }}</span>
                </label>
                <input type="email" v-model="email" :maxlength="max_length" required>
            </div>
            <div class="form_group">
                <label for="password">Password
                    <span> {{ password.length + '/' + max_pass_length }}</span>
                </label>
                <input type="password" v-model="password" :maxlength="max_pass_length" required>
            </div>
        </form>


        <div v-if="error.length > 0"> {{error}}</div>
    </div>
    <!-- Add tweet -->
    <div class="tweetBox" v-else>
        <h2>Welcome {{ userData.name }} write your first Tweet</h2>

    </div>

</div>

```
Here in the HTML, since we used the v-else on the add tweet section and used the local storage to retrieve the data submitted by the user we can use an in-template expression to grab the user name so that we can output a welcome message. 

In the next video we will create a tweet box form so that after the registration the user can write a tweet.

-------------------------------------------------------------- END VIDEO 1 ------------------------------------------------------------------

### Create a tweets box form - HTML
Watch the video on [YouTube](https://youtu.be/xFwfrIciFt0)
Now it's time to build our add tweet form, we did something very similar earlier in the course but now we will need store and make the data persistent so that we can show a list of tweets even when the page refreshes.

```html
<div class="tweetBox" v-else>
    <h2>Welcome {{ userData.name }} write your first Tweet</h2>
    <form v-on:submit.prevent="sendTweet">
        <div class="form_group">
            <label for="tweet">
                Send your tweet
                <span> {{ tweetMsg.length + '/' + max_tweet }}</span>
            </label>
            <textarea name="tweet" id="tweet" cols="30" rows="10" v-model="tweetMsg" maxlength="200"></textarea>
        </div>
        <button type="submit">Tweet</button>
    </form>

</div>
```
This is nothing new to us now. Inside the `tweetBox` element we add a form with the usual v-on directive and,
a method  `sendTweet` that we will need to define inside the methods object. This will take the tweet and save it somewhere, maybe in a property in the data object.

Inside the form there is a `textarea` that has a `v-model` directive that binds it to a `tweetMsg` property that we need to create.

Finally a submit button.

We also have a span inside the tweet label that shows a characters counter to the user, like we did before in the registration form. 
Here we have a new property `max_tweet` that is used to show the limit and the `tweetMsg.length` used to show the current number of character inserted.

### Create a tweets box form - VUE
Let's go to the Vue instance and add the properties and the `sendTweet` methods.

Our data object now has three more properties, the `max_tweet` set to `200`, the `tweetMsg` that binds to the `textarea` and a `tweets` array that we will use to store all tweets the user sends.

```js
data: {
    userData: {},
    usersID: 0,
    name: "",
    email: "",
    password: "",
    max_length: 25,
    max_pass_length: 16,
    max_tweet: 200, // max tweets lenght
    error: "",
    registered: false,
    tweetMsg: "", // current tweet
    tweets: [] // list of tweets
}

```
Inside the methods we have a new method that will be invoked by the v-on directive when the form is submitted

```js

sendTweet(){
    /* Store the tweet in the tweets property */
    this.tweets.unshift(
        {
            text: this.tweetMsg,
            date: new Date().toLocaleTimeString()
        }

    );
    /* Empty the tweetMsg property */
    this.tweetMsg = "";
    //console.log(this.tweets);

    /* Tranform the object into a string  */
    stringTweets = JSON.stringify(this.tweets)
    //console.log(stringTweets);

    /* Add to the local storage the stringified tweet object */
    localStorage.setItem('simple_tweet_tweets', stringTweets)
},

```

The code above does 4 things
- takes the tweets array and adds in it an object to represent a single tweet with a text and date properties. To the text property we assign the value of the `tweetMsg` that is bound with the `textarea` and for the date we create a new date object with the `new Date().toLocaleTimeString()` method.
- we empty the value of the `tweetMsg`
- we transform the tweets property into a string using the method `JSON.stringify(this.tweets)` 
- Then we add it to the local storage.

Our final main.js file now looks like that:

```js

let app = new Vue({
    el: '#app',
    data: {
        userData: {},
        usersID: 0,
        name: "",
        email: "",
        password: "",
        max_length: 25,
        max_pass_length: 16,
        error: "",
        registered: false,
    },
    
    methods: {
          registerAccount(){
      
              if (this.name.length > 0 && this.name.length <= this.max_length && this.email !== "" && this.password !== "" ) {
                  
                    this.userData.id = ++this.usersID,
                    this.userData.name = this.name,
                    this.userData.email = this.email,
                    this.userData.password = this.password
                    this.registered=true;
                
                  
                 
              } else {
                  this.error = "Complete all the form fields"
              }
            
            /* Add registration data to the local storage */
            localStorage.setItem('simple_tweet_registered', true)
            /* Add the whole userData object as JSON string */
            localStorage.setItem('simple_tweet_registered_user', JSON.stringify(this.userData))
            
            /* Clear the registration inputs */
            this.name = "";
            this.email = "";
            this.password = "";
        },
        sendTweet(){
            /* Store the tweet in the tweets property */
            this.tweets.unshift(
                {
                    text: this.tweetMsg,
                    date: new Date().toLocaleTimeString()
                }

            );
            /* Empty the tweetMsg property */
            this.tweetMsg = "";
            //console.log(this.tweets);

            /* Tranform the object into a string  */
            stringTweets = JSON.stringify(this.tweets)
            //console.log(stringTweets);

            /* Add to the local storage the stringified tweet object */
            localStorage.setItem('simple_tweet_tweets', stringTweets)
        },

        
    },
    created(){
        /* Check if the user is registered and set the registered to true */
        if(localStorage.getItem("simple_tweet_registered") === 'true'){
            this.registered = true;
        }
        // repopulate the userData object
        if(localStorage.getItem('simple_tweet_registered_user')) {
            this.userData = JSON.parse(localStorage.getItem('simple_tweet_registered_user'))
        }
       
    }

});

```
Now that we competed this part we can show a list of tweets and also take care of what to do when the page is refreshed and the local Storage has in it our tweets object. We will need to parse it back and add its content to the `tweets` property to see the list.

Next we will show the list of tweets using a v-for directive

--------------------------------------------------- END VIDEO 2 ----------------------------------------------------------------------
### Show a list of tweets - HTML
Watch the video on [YouTube](https://youtu.be/3DzBkUHH3bU)
Inside our root element
```html
 <!-- Show all tweets -->
    <div class="card_tweets">
        <section class="tweets" v-if="tweets.length > 0">
            <h2>Tweets</h2>
            <div class="tweetMsg" v-for="(tweet, index) in tweets">
                <p>
                    {{tweet.text}}
                </p>

                <div class="tweetDate">
                    <i class="fas fa-calendar-alt fa-sm fa-fw"></i>{{tweet.date}}
                </div>
 
            </div>

        </section>
        <div v-else>No tweets to show</div>
    </div>
```
Here we wrap everything in a div with a class `card_tweets`
then we use a v-if directive inside a child section to check if there are tweets in the `tweets` array `v-if="tweets.length > 0"`. Inside this section we can loop over the tweets array using a `v-for="(tweet, index) in tweets"` directive. After that we user in-template expressions to show the tweet text property `{{tweet.text}}` and the data `{{tweet.date}}`.
After the `section` we can use a `v-else` directive to show a message in case there are no tweets stored inside the tweets array `<div v-else>No tweets to show</div>`. 
Done. Now one last thing we need to do, and it's to figure out what to do to remove tweets from the list.

But when the user refresh the page everything is gone so we need to work with the `localStorage` once again to repopulate our tweets array from it before rendering the root element.

Inside the `created` life cycle hook we will now write some code to parse the tweets and save them back in the `tweets` property

```js
/* Parse all tweets from the local storage  */
if(localStorage.getItem("simple_tweet_tweets")) {
    console.log("There is a list of tweets");
    this.tweets = JSON.parse(localStorage.getItem('simple_tweet_tweets'))
}else {
    console.log("No tweets here");
}

```
Here we used the `localStorage` api to first check if there is a key with called `simple_tweet_tweets` if so we grab the tweets property using `this.tweets` and assign to it the content of the `localStorage` but we parse the string back to `JSON` with `JSON.parse` so  we write `this.tweets = JSON.parse(localStorage.getItem('simple_tweet_tweets'))`

Now everything works, after we refresh the page tweets are still there. Let's move forward. In the next step we will add a method to remove tweets from the list.

### The user can remove tweets
Inside the div that contains the tweet message we can add another div to show a link and a trash icon so that when the user clicks it we remove that tweet. 

```html
<div class="tweet_remove" @click="removeTweet(index)">
    <span class="remove">Delete this tweet <i class="fas fa-trash fa-xs fa-fw"></i></span>
</div>

```
Here we simply used a v-on short syntax directive on the div and invoked a method `removeTweet(index)` passing to the method the element index so that we know what to remove.

Let's build our `removeTweet` method now

```js
removeTweet(index){
    let removeIt = confirm("Are you sure you want to remove this tweet?")
    if(removeIt) {
        this.tweets.splice(index, 1);
        /* Remove the item also from the local storage */
        localStorage.simple_tweet_tweets = JSON.stringify(this.tweets)
    }
}

```
This bit of code is very straight forward. Our method accepts an index that represents the position of the tweet object in the array obtained from the v-for directive when the method is invoked. We then create a variable to ask to the user to confirm he wants to delete the tweet and used the `confirm` function for that. If the value of the `removeIt` variable is true then we execute the code and use `this.tweets.splice(index, 1)` to remove the tweet from the array using its index and finally we update the `localStorage` value by assigning to is the new array using the `localStorage.simple_tweet_tweets = JSON.stringify(this.tweets)`.

### Final Code
Our code is now complete, you can find the final code below or inside the repository [https://bitbucket.org/fbhood/simple-tweet-app/src/master/]

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vue 2 Hello World</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
        integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <!-- Axios CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.0/axios.min.js"
        integrity="sha512-DZqqY3PiOvTP9HkjIWgjO6ouCbq+dxqWoJZ/Q+zPYNHmlnI2dQnbJ5bxAHpAMw+LXRm4D72EIRXzvcHQtE8/VQ=="
        crossorigin="anonymous"></script>
    <!-- development version, includes helpful console warnings -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
        integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body>
<div id="app">
    <div class="card">
        <i class="fab fa-twitter fa-lg fa-fw"></i>
        <!-- Register an account -->
        <div class="register" v-if="!registered">
            <button form="register" type="submit">Register</button>
            <h2>Create your account</h2>
            <form id="register" v-on:submit.prevent="registerAccount">
                <div class="form_group">
                    <label for="name">Name
                        <span> {{ name.length + '/' + max_length }}</span>
                    </label>
                    <input type="text" v-model="name" :maxlength="max_length" required>
                </div>
                <div class="form_group">
                    <label for="email">Email
                        <span> {{ email.length + '/' + max_length }}</span>
                    </label>
                    <input type="email" v-model="email" :maxlength="max_length" required>
                </div>
                <div class="form_group">
                    <label for="password">Password
                        <span> {{ password.length + '/' + max_pass_length }}</span>
                    </label>
                    <input type="password" v-model="password" :maxlength="max_pass_length" required>
                </div>
            </form>


            <div v-if="error.length > 0"> {{error}}</div>
        </div>
        <!-- Add tweet -->
        <div class="tweetBox" v-else>
            <h2>Welcome {{ userData.name }} write your first Tweet</h2>
            <form v-on:submit.prevent="sendTweet">
                <div class="form_group">
                    <label for="tweet">
                        Send your tweet
                        <span> {{ tweetMsg.length + '/' + max_tweet }}</span>
                    </label>
                    <textarea name="tweet" id="tweet" cols="30" rows="10" v-model="tweetMsg" maxlength="200"></textarea>
                </div>
                <button type="submit">Tweet</button>
            </form>

        </div>

    </div>
    <!-- Show all tweets -->
    <div class="card_tweets">
        <section class="tweets" v-if="tweets.length > 0">
            <h2>Tweets</h2>
            <div class="tweetMsg" v-for="(tweet, index) in tweets">
                <p>
                    {{tweet.text}}
                </p>

                <div class="tweetDate">
                    <i class="fas fa-calendar-alt fa-sm fa-fw"></i>{{tweet.date}}
                </div>
                <div class="tweet_remove" @click="removeTweet(index)">
                    <span class="remove">Delete this tweet <i class="fas fa-trash fa-xs fa-fw"></i></span>
                </div>
                
            </div>

        </section>
        <div v-else>No tweets to show</div>
    </div>
</div>
<script src="./main.js"></script>
</body>

</html>

```

JavaScript file

```js

let app = new Vue({
    el: '#app',
    data: {
        userData: {},
        usersID: 0,
        name: "",
        email: "",
        password: "",
        max_length: 25,
        max_pass_length: 16,
        max_tweet: 200,
        error: "",
        registered: false,
        tweetMsg: "",
        tweets: []
    },
    
    methods: {
          registerAccount(){
      
              if (this.name.length > 0 && this.name.length <= this.max_length && this.email !== "" && this.password !== "" ) {
                  
                    this.userData.id = ++this.usersID,
                    this.userData.name = this.name,
                    this.userData.email = this.email,
                    this.userData.password = this.password
                    this.registered=true;
                
                  
                 
              } else {
                  this.error = "Complete all the form fields"
              }
            
            /* Add registration data to the local storage */
            localStorage.setItem('simple_tweet_registered', true)
            /* Add the whole userData object as JSON string */
            localStorage.setItem('simple_tweet_registered_user', JSON.stringify(this.userData))
            
            /* Clear the registration inputs */
            this.name = "";
            this.email = "";
            this.password = "";
        }, 
        sendTweet(){
            this.tweets.unshift(
                {
                    text: this.tweetMsg,
                    date: new Date().toLocaleTimeString()
                }

            );
            this.tweetMsg = "";
            
            //console.log(this.tweets);
            stringTweets = JSON.stringify(this.tweets)
            //console.log(stringTweets);
            localStorage.setItem('simple_tweet_tweets', stringTweets)
        },
        removeTweet(index){
            let removeIt = confirm("Are you sure you want to remove this tweet?")
            if(removeIt) {
                this.tweets.splice(index, 1);
                /* Remove the item also from the local storage */
                localStorage.simple_tweet_tweets = JSON.stringify(this.tweets)
            }
        }
    },
    created(){
        /* Check if the user is registered and set the registered to true */
        if(localStorage.getItem("simple_tweet_registered") === 'true'){
            this.registered = true;
        }

        if(localStorage.getItem('simple_tweet_registered_user')) {
            this.userData = JSON.parse(localStorage.getItem('simple_tweet_registered_user'))
        }
        /* Parse all tweets from the local storage  */
        if(localStorage.getItem("simple_tweet_tweets")) {
            console.log("There is a list of tweets");
            this.tweets = JSON.parse(localStorage.getItem('simple_tweet_tweets'))

        }else {
            console.log("No tweets here");
        }
    }

});

```

We are ready to move forward with our Vue journey, it's now time to learn about components. 


---------------------------------------------------------- END VIDEO 3 ---------------------------------------------------------------------