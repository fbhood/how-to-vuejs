# Continuos Deployment with BitBucket and Netlify
Watch the video on [YouTube](https://youtu.be/BH5I68DzcYQ) 

Checkout the final repositories on BitBucket: 
- [SimpleTwitter](https://bitbucket.org/fbhood/simple-twitter/src/master/)
- [VuePortfolio](https://bitbucket.org/fbhood/vue-folio/src)

The final step is to deploy our projects so that others can see them. To do that we will use two services:
- BitBucket a git-based source code repository hosting (you can use gitHub if you prefer)
- Netlify a web hosting company that provides hosting for websites that have source code files stored in a Git version control system.


## Create projects folders and copy all files
First, create two folders one for each project:
- vue-folio
- simple-twitter
then copy all projects files in the related folder.

## Initialize a git repository
Next, we need to initialise the git repository locally.

```
cd vue-folio 
git init
git add .
git commit -m"Initial Commit"
```
The commands above needs to be executed in a termial, you need to have git installed on you system, if you don't read [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

With the first command we navigate to the project folder called `vue-folio`, then we initialise a git repository, 
add all files to the stagin area and commit the files.

Repeat the steps above for both projects folders!

## Create a BitBucket or GitHub repository
I assume you already have an account with GitHub o BitBucket, if you don't then go over and create one.
Follow the steps in the video to create the repositories and connect them with you local repositories.


## Create an account with netlify and create a site
Netlify's free plan can be used for private projects, hobby websites and experiments, it's a perfect fit for our tutorial. Follow the steps in the video to deploy your projects there.

