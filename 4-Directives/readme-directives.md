## Directives 
Click to view the [Repository](https://bitbucket.org/fbhood/how-to-vuejs/src/master/4-Directives/)
| Click to view the [YouTube-Video](https://youtu.be/LICvNmhsTEs)

Inside our html files we can use directives to interact with html attributes. A directive applies effects to the DOM when its expression changes. 

### The v-bind directive on HTML attributes
We have used the {{}} syntax to render something between HTML opening and closing tags, but inside an HTML tag we cannot use the {{ }} syntax. So how to we connect an HTML attribute to the Vue instance? We use the v-bind directive instead and it let's us access data object properties like we have done before. The v-bind directive is one of the directives that takes arguments and there are specified after the colon. In our case here what's specified after the colon is the HTML attribute name like id, class, href, src etc. 

If we need to dynamically assign an attribute like href or even a class we can bind it with the Vue instance using the v-bind directive. It will then be able to get what's in the options object, like properties in the data object.
Let's see v-bind in action and start connecting the `id` and `class` attributes so we can assign them values dynamically with Vue.

Inside our index.html file
```html
<div id="app">
    <div v-bind:class="dynamicClass" v-bind:id="dynamicId">Dinamically assign a class and an id to the div</div>
</div>
```
Let's break the code above and see what it's doing. 
First we have a `div` tag inside the root element, then we use the v-bind directive on the class and the id attributes. Inside the quotes we specify two properties that later we will define inside the data object of our Vue instance.

Remember that when using Vue directives the content between quotes is treated as a JavaScript expression.

Let's define these two properties inside the Vue instance.
```js
let app = new Vue({
    el: "#app",
    data: {
        title: "John Doe Portfolio", 
        titleHTLM : "John Doe <span class='badge'>Portfolio</span>",
        projects: [
            {title: "portfolio", languages: ["HTML", "CSS", "VueJS"]},
            {title: "grocery shop", languages: ["HTML", "CSS", "PHP"]},
            {title: "blog", languages: ["HTML", "CSS", "PHP"]},
            {title: "automation script", languages: ["Python"]},
            {title: "eCommerce", languages: ["HTML", "CSS", "PHP"]},
        ],
        dynamicId : "projects_section",
        dynamicClass : "projects"
    }
})


```
So we have defined `dinamicId: "projects_section"` and `dynmicClass: "projects"` properties and assigned them two values.

Thanks to the data binding on the attributes our HTML tag will be rendered as follow and we can now dynamically change our attributes values and see them change reactively

```html
<div id="projects_section" class="projects">Dynamically assign a class and an id to the div</div>
```

### V-bind with Boolean values
With attributes using a boolean value, the v-bind directive works differently. It will show the attribute only if the property's value is true. 
In all other cases it won't render the attribute and its content. 

For the next example we will use a button with the disabled attribute.
Inside our root HTML element:
```html
    <div id="app">
        <button v-bind:disabled="disabled">You can't click this button</button>
    </div>
```
Inside a Vue instance:
```js
let app = new Vue({
    el: '#app',
    data: {
    //disabled: false, // wont render the attribute
    //disabled: null, // wont render the attribute
    //disabled: undefined, // wont render the attribute
    disabled: true // renders the attribute
}
})

```

Only if the disabled property is set to true the attribute becomes visible and renders its property content.
```html
    <button disabled>You can't click this button</button>
```
This is something to keep in mind when working with such attributes.

Another thing to consider is that bindings can include a single JavaScript expressions, with some restrictions:
- only expressions are allowed
- only a single expression
- no statements
- no flow control tools, but the ternary operator works.

If you want to read more, visit the documentation 
here:[https://vuejs.org/v2/guide/syntax.html#Using-JavaScript-Expressions]

So far we have seen only two Vue directives, v-html and v-bind. 
There are a number of directive available, to list just a few:
- v-html
- v-bind
- v-if
- v-else-if
- v-else
- v-for
- v-on
All directives have a v- prefix, however there are shorthands for the v-bind (:) and v-on (@). 

They work in the same way, here a quick reference for the v-bind and v-on directives: 
```html
<!-- Long syntax -->
<a v-bind:href="url">Some link</a>
<!-- Shot syntax -->
<a :href="url">Some link</a>
<!-- Long syntax with dynamic arguments -->
<a v-bind:[attribute_name]="url">Some link</a>
<!-- Shot syntax with dynamic arguments -->
<a :[attribute_name]="url">Some link</a>
```
Shorthand for v-on
```html
<!-- Long syntax -->
<a v-on:click="runFunction">Some link</a>
<!-- Shot syntax -->
<a @click="runFunction">Some link</a>
<!-- Long syntax with dynamic arguments -->
<a v-on:[attribute_name]="runFunction">Some link</a>
<!-- Shot syntax with dynamic arguments -->
<a @:[attribute_name]="runFunction">Some link</a>

```
Now let's see what is a dynamic argument.

### Dynamic arguments
Directives can have dynamic arguments since Vue 2.6.0. We can use a JavaScript expression in the directive argument if we wrap it inside square brackets.
There are some restrictions:
- expressions should evaluate to a string
- spaces and quotes are invalid

Lets see a practical example
```html
    <a v-bind:[attribute_name]="url">Visit my Website</a>
```
Inside our data object we can define the directive arguments as they were properties, where the property value is the name our our HTML attribute like `hef` in the following example:

```js
let app = new Vue({
    el: '#app',
    data: {
        attribute_name: 'href',
        url: 'https://fabiopacifici.com'
    }
})

```
The code above renders the attribute name `href` and its value dynamically when we bind it using the v-bind directive.

The result will surely be this:  
```htm
<a href="https://fabiopacifici.com">Visit my Website</a>
```

### Dynamic events
The same concept can be applied to events directives like the v-on.
This directive do the job of the JavaScript event listener. v-on accepts an argument like click for example `v-on:click="doSomething"`.

To apply the concept of dynamicity let's create a v-on directive and use the square brackets after it to specify a dynamic event.

Inside the index.html file we will place the following code:
```html
    <div id="app">
        <a v-on:[event_name]="runFunction">Some link</a>
    </div>
```
Let's break the code above, first we have our root element the `div` with an `id` of `app`. Inside the root element we added an anchor tag `<a>Some link</a>`. 
The anchor tag has a `v-on` directive in it. After the directive we specify a dynamic argument `v-on:[event_name]` where `event_name` will be a property inside our vue instance that we can change as we need. The v-on directive works like any event listener, so between quotes we need to specify the name of the function that we want to run when the event is triggered, therefore `runFunction`.

Now, inside our main.js file
```js
let app = new Vue({
    el: '#app',
    data: {
    event_name: "click"
    },
    methods: {
        runFunction() {
            console.log("test click function");
        }
    }
})
```
Let's review what the code above does. First we create the Vue instance. 
Then we add the `event_name` property inside the data object and we assign to it a value of `click` this is the event we will listen for.
Finally, we said that the v-on directive runs a function when the event is triggered, therefore we need to write a method inside our Vue instance. So inside the methods object we create a new function called `runFunction` that will simple output a message inside the console. 

The power of dynamic events its clear when we replace the value of the `event_name` property with a different event name.




