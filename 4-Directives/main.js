
/* let dataObject = {

} */

// Vue JS goes here
let app = new Vue({
    // options object
    el: '#app',
    data: {
        title: "John Doe Portfolio", 
        titleHTML : "John Doe <span class='badge'>Portfolio</span>",
        projects: [
            {title: "portolio", desc: "Lorem ipum"},
            {title: "twitter clone", desc: "Lorem ipum"}
        ],
        dynamicID: "projects_section",
        dynamicClass: "projects",
        disabled: true,
        attribute_name: 'href',
        url: 'https://fabiopacifici.com',
        event_name: "click"
    }, 
    methods: {
        runFunction(){
            console.log("Test click function");
        }
    }
});