## Loops
Click to view the [Repository](https://bitbucket.org/fbhood/how-to-vuejs/src/master/7-loops/])
 | Click to view the video on [YouTube](https://youtu.be/aViHg80-7Bs)

Let's go back to our projects example and learn how to use the v-for directive to output each project of the array onto the page. For our next task it would be useful if we could use a loop, and the v-for directive is here to help. 
It's syntax has not much in commune with a classic `for` loop in JavaScript but more with a Python `for in` loop or with the `for in` JavaScript loop used to iterate over objects.
With this directive we specify between quotes the elements of the array and the single element using the syntax `project in projects` where projects is our property inside the data object that contains an array of objects and project is the single element of the array. We can call this as we like, the thing to keep in mind is that what follows the `in` keyword must be an iterable from our data object while what comes before can be anything we like to refer to each element of the iterable. In our case project seems the most appropriate choice since we have an array of projects.

Our javascript file is the following
```js
let app = new Vue({
    el: "#app",
    data: {
        name: "John Doe",
        title: "Portfolio",
        projects: [
            {title: "portfolio", languages: ["HTML", "CSS", "VueJS"]},
            {title: "grocery shop", languages: ["HTML", "CSS", "PHP"]},
            {title: "blog", languages: ["HTML", "CSS", "PHP"]},
            {title: "automation script", languages: ["Python"]},
            {title: "eCommerce", languages: ["HTML", "CSS", "PHP"]},
        ],
        
    }

});
```
Now inside the html file let's use the v-for to render the title of each project.
```html
    <div id="app">
        <h1>{{name}} {{title}}</h1>
        <ul>
            <li v-for="project in projects">{{project.title}}</li>
        </ul>
    </div>
```
In the code above we used `{{name}} {{title}}` to render a main title for our portfolio. Then we used the v-for directive and specified inside the quests that we want to assign each element of the iteration to a project variable `v-for="project in projects"`
now on each iteration the `project` variable holds an object from which we can retrieve its properties using the dot notation like so `{{ project.title }}`

One thing to note is that the v-for directive gives us also access to the index of the element at each iteration. We can store it in a variable like we did with the single element we called project. To do that we need to wrap them between parentheses and separate the element and its index with a comma, like so `v-for="(project, index) in projects"`. 

Also note that when working with objects Vue can show an alert to inform that the use of a key is recommended. What this mean is that its expected a key to identify each element when it's rendered. This can be done using the `key` attribute and bind it for instance to an id property on the object or to another different property, like so

```html

<div id="app">
    <h1>{{name}} {{title}}</h1>
    <ul>
        <li v-for="project in projects" :key="project.title">{{project.title}}</li>
    </ul>
</div>
```
Here we used the v-bind shot hand directive to bind the key attribute to the project.id property if exists or another property. 

```js
let app = new Vue({
    el: "#app",
    data: {
        name: "John Doe",
        title: "Portfolio",
        projects: [
            {title: "portfolio", languages: ["HTML", "CSS", "VueJS"]},
            {title: "grocery shop", languages: ["HTML", "CSS", "PHP"]},
            {title: "blog", languages: ["HTML", "CSS", "PHP"]},
            {title: "automation script", languages: ["Python"]},
            {title: "eCommerce", languages: ["HTML", "CSS", "PHP"]},
        ],
        
    }

});

```

The v-for can also be used to iterate over objects. It such case we have access to the value, the key and also the index like so `v-for="(value, key, index) in object"` where object is a property in the data object.

If you want to learn more visit the documentation here: [https://vuejs.org/v2/guide/list.html]

Let's now move to another important feature of VueJs that is how to handle user inputs and events.

