// Vue JS goes here
let app = new Vue({
    // options object
    el: '#app',
    data: {
        name: "John Doe",
        title: "Portfolio",
        projects: [
            {title: "Portfolio", language: ["HTML", "CSS","JavaScript", "VueJS"]},
            {title: "Twitter clone", language: ["HTML", "CSS","JavaScript", "VueJS"]},
            {title: "Blog", language: ["HTML", "CSS","JavaScript", "VueJS"]},
            {title: "eCommerce", language: ["HTML", "CSS","JavaScript", "VueJS"]}

        ]
    }, 
    methods: {
       
    }
});