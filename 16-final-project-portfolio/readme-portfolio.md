# Final Project | build a Portfolio with VueJS, VueRouter, Axios, GitHub API and deploy to netlify 
Watch on [YouTube](https://youtu.be/I6hQnWQU4rQ) | Check the repositori on [BitBucket](https://bitbucket.org/fbhood/how-to-vuejs/src/master/16-final-project-portfolio/)

We are ready to build the final project of this course. For our vue-folio we will start from where we left
the previous video. We will build a single-page application that has two routes one for the home page
and one for the projects page. 

Below our building blocks:
- vuejs 
- vue router
- axios
- gitHub rest api
- portfolio design

## Project structure

To speed things up we will just copy the code we wrote in the previous video.
The project structure will be the following
```
|-- index.html
|-- assets/
    |-- css/
        |-- style.css
    |-- js/
        |-- main.js
    |-- img/
```
### index.html file
The index.html file is a little different from what we had in the previous video. Here, we will place only
the router-view component that is responsible to show the component matching a given route. 
Then we will place the actual route-links inside each component to make sure we have the desired result
as per the design.

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vuefolio</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;300;400;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
        integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <link rel="stylesheet" href="./assets/css/style.css">
</head>

<body>

    <div id="app">

        <!-- Render the component for the corresponding route -->
        <router-view></router-view>

    </div>
    <footer> © Developed by <a href="https://fabiopacifici.com">Fabio Pacific</a> </footer>
    <!-- Axios -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <!-- VueJS development version -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <!-- Vue Router -->
    <script src="https://unpkg.com/vue-router@2.0.0/dist/vue-router.js"></script>
    <!-- Main Js file -->
    <script src="./assets/js/main.js"></script>
</body>

</html>
```

### style.css file
Since this is not going to be a css tutorial for the CSS part you can simply copy the code from the repository file if you are following along.
    ```css
    /* Utility Classes */
    .d_none {
        display: none;
    }
    .d_flex {
        display: flex;
    }
    .container {
        max-width: 1170px;
        margin: auto;
    }
    a {
        color: white;
    } 
    a:hover {
        color:#DB5461;
        
    }
    .loading {
        font-size: 2rem;
    }
    /* END Utility Classes */
    /* Components */
    .bio__media {
        display: flex;
        justify-content: flex-start;
        align-items: center;
        text-align: left;
    }
    .bio__media img {
        height: 120px;
    }
    .bio__media__text {
        padding: 1rem;
    }
    .bio__media__text h1{
        font-size: 36px;
        font-weight: 900;
        color: #DB5461;

    }
    .bio__media__text p {
        font-weight: 100;
        font-size: 16px;
        line-height: 1.5rem;
        
    }

    .card__custom {
        position: relative;
        display: flex;
        max-width: 400px;
        height: 300px;
        min-height: 300px;
        padding: 0.5rem;
        margin-bottom: 3rem;
        flex-grow: 1;
        flex-basis: calc(100% /2);
        align-items: center;
        justify-content: space-between;
    }
    .card__custom > .card__custom__text {
        max-width: calc((100% / 3) *2);
        text-align: right;
        height: 80%;
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        overflow: hidden;

    }
    .card__custom__img {
        
        position: absolute;
        width: 70%;
        height: 100%;
        background-image: url(../img/cards_bg_img.svg);
        background-position: center;
        background-repeat: no-repeat;
        background-size: contain;
        display: inline-block;
        z-index: -1;
        left: 60%;
        transform: translateX(-50%);
        border-radius: 85px 0 100px 25px;

    }
    .card_custom__button a, .btn_load_more {
        background: #F1EDEE;
        border: 5px solid #3D5467;
        box-sizing: border-box;
        border-radius: 54px;
        padding: 0.5rem 1rem;
        font-weight: 900;
        color: #3D5467;
    }
    .card_custom__button a:hover, .btn_load_more:hover {
        cursor: pointer;
        background: #324555;
        color: white;
        border-color: #DB5461;
        transition: 1s;
    }
    .card__custom__text h3 {
        text-transform: uppercase;
        font-size: 1.5rem;
    }
    /* END Componenet */
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    body{
        font-family: 'Raleway', Arial, Helvetica, sans-serif;
        color: white;
        background: linear-gradient(116.82deg, #3D5467 0%, #1A232B 99.99%, #333333 100%);

    }
    a {
    text-decoration: none;   
    }

    /* Home Page */
    main#home {
        width: 100%;
        height: 100vh;
        min-height: 600px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    #home > .about__me {
        text-align: center;
        width: 80%;
        line-height: 1.5rem;
    }
    #home > .about__me > h1 {
        margin: 20px 0 0;
        font-size: 36px;
        font-weight: 900;
        color: #DB5461;
    }
    #home > .about__me > h3 {
        font-size: 28px;
        font-weight: 500;

    }
    #home > .about__me > h1, #home > .about__me > h3  {
        font-style: normal;
        line-height: 42px;
        letter-spacing: 0.115em;

    }
    #home > .about__me p {
        font-weight: 100;
        font-size: 22px;
        padding: 2rem;
    }
    .skills_projects_link {
        position: relative;
    }
    .skills_projects_link > a {
        text-transform: uppercase;
        color: white;
        font-weight: 900;
        font-size: 18px;
        line-height: 21px;

    }
    .skills_projects_link > a:hover {
        color: #DB5461;
        transition: all 0.5s ease-in-out;

    }
    .skills_projects_link > a:hover::after {
        position: absolute;
        left: 50%;
        transform: translateX(-50%);
        display: flex;
        margin: auto;
        text-align: center;
        content: "";
        width: 30px;
        height: 2px;
        background-color: #DB5461;
        transition: background-color 0.5s ease-in-out;

    }

    /* Header */
    #site_header {
        text-align: center;
        padding: 2rem 0;
        justify-content: space-between;
        align-items: center;
    }
    #site_header > h1 {
        text-transform: uppercase;
    }
    nav a {
            color: #e2e2e2;
        text-transform: uppercase;
        font-weight: 900;
    }
    nav a:hover {
        color: #DB5461;
    }
    /* Portfolio Page Section */

    #portfolio {
        margin-top: 4rem;
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        justify-content: space-around;
    }
    .btn_load_more {

    }
    /* Skills */

    #skills_section {
        margin-top: 4rem;
        min-height: 300px;
        background-image: url(../img/skills_bg.svg);
        background-repeat: no-repeat;
        background-size: contain;
        background-position: top left;
    }
    #skills_section h2 {
        margin-left: 180px;
        font-size: 44px;
        color: #F1EDEE;
        line-height: 2rem;

    }
    #skills_section ul {
        list-style: none;
        margin: 20px 120px;
        display: flex;
        flex-wrap: wrap;

    }
    #skills_section ul  li {
        padding: 1rem;
        margin: 0.5rem;
        background-color: #DB5461;
        border: 5px solid #3D5467;
        border-radius: 35px;
    }



    .avatar {
        width: 30px;
        height: auto;
        border-radius: 50%;
            margin: 0 1rem;

    }




    .card__back {
        display: none;
    }
    .rotate__card {
        transform: rotate3d(360,0,0,180deg);
    }
    /* Site Footer */

    footer {
        text-align: center;
        padding: 2rem 0;
    }



    /* Media Query  */

    @media screen and (max-width: 475px) {
        .card {
            flex-basis: 100%;
            width: 100%;
        }
    }
    ```

## Main.js file basic structure
Inside the main.js file we have the core of our single page application. 
Here we will define the route components that needs to be rendered for each view/page, the homepage and projects components. 

Then we will define two routes, one for the homepage and one for the projects page, create a router instance and pass to 
it our routes, finally we will create a new vue instace and, pass to it the router instance and, mount on it the root html element.

Let's start with the route components!

### Define components for each view
The homepage component is fairly simple.  
```js
// Homepage component
const Home = {
    template: 
    `<main id="home">
        <div class="about__me">
            <img src="./assets/img/avatar.svg" alt="">
            <h1>John Doe</h1>
            <h3>Python Expert</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
    
            <div class="skills_projects_link">
                <router-link to="/projects">Projects/Skills</router-link> 
            </div>
        </div>
    </main>`
}
```
Let's break it down. First we create a Home constant that will hold the router component object. Inside the object the only thing we 
will put is the template property with some markup to render our page. The main thing to notice here is the router-link component
that will point to the /projects route from the homepage.


Next let's create a route component for the projects page. We have a lot to do here so for now let's 
just add some boilerplate code, we will come back to it later. And write step by step all the logic. 

```js
const Projects = {
    template: 
    `<div>
        <h1>Projects</h1>
    </div>`,
    data() { 
        return {
                // Data object here
            }
    },
    methods: {
        // All methods here
    },
    mounted(){  
        // Lifecycle hook      
        
    }
}

```
The Projects route components has a `template` property that so far holds a basic markup that only spit out an `h1` title.
After that there is the component's data method that returns an empty object, then an empty methods object and an empty lifecycle hook.
That's all we need for now, let's move forward and define the rest of the building blocks, the routes, the router and the vue instances.


### Define routes, the router and, the vue instance
Now that we have two components to render our main pages we can move forward to the next steps:
- Define routes
- create the router instance
- create and mount the vue instance

Fist let's define our two routes and link the components.
```js

// Define routes
const routes = [
    {path: '/', component: Home},
    {path: '/projects', component: Projects},
];

```
In the code about we have defined a new constant called routes. In it we defined two routes as an array of objects. 
Each object has two properties:
- path
- component

The first object is for the homepage, its path will responds to requests made to our website base url, like https://fabiopacifici.com/ 
then the component property links this page to the route's component called `Home` that we defined in the previous step. 

The second object is for the projects page, the path responds to requests made to `/projects` and its linked to the 
`Pojects` route component.

Now that we have our routes, we can create a new Vue router instance and, pass to it this two routes.

```js

// create the router instance
const router = new VueRouter({
    routes
})

```
Above we used the ES6 sintax that allows us to just put the name of the variable holding the routes since it is equal to the name 
of the property that we needed to use. It's actually the same as writing `routes: routes`.

Now, we create a vue instance, inject the router instance inside it and, finally mount the root element. 

```js

// create and mount the vue instance
const app = new Vue({
    router
}).$mount('#app');
```

Done! We now have everything in place to start building our portfolio and, complete the Projects route component.

## Build the main Projects route component
We will start woking in the data object, here we need to define properties that will hold all our projects once we fetch data
from the git hub api.
### The data object
To keep things easy I have intentinally limited results to 20, if you feel this isn't enough feel fee to 
change the code as you like. You can implement a pagination for you results by increasing the page property that will be passed to the query string or return more results per page by increasing the value
of the `perPage` property.
```js
data() { 
    return {
        projects: [],
        projectsList: null,
        skills: [],
        projectsCount: 5,
        perPage: 20,
        page: 1,
        loading: true,
        errors: false,
        }
    },
```
As we learned in the chapter where we used axios to fetch data from the gitHub REST API, there are a few properties we need to define. 
The component's data function returns an object with a `projects` property where we will store all projects we fetch from gitHub.
Then we add a `projectsList` property that holds only few projects at the time, we will use this property later to implement a very simple
load more feature in combination with the `projectsCount` property.
Then we have a `skills` property where we will store all languages used to build our projects. 
The `perPage: 20` and `page: 1` properties will be used to build the query string used to fetch data from gitHub, it will take 20 projects and 
return only the first page of results unless we change this values obviously.

Finally, we have a `loading: true` property that we will use to check if the page is fetching data and an `errors: false` property used to show an error message in case we are unable to connect to the gitHub server.

In the next step we will start working on all methods required to make our application works.
### The Methods
The first method is the one we will use to fetch data from gitHub.
#### Fetch all data

```js

 fetchData: function(){
            axios
            .get(`https://api.github.com/users/fbhood/repos?per_page=${this.perPage}&page=${this.page}`)
            .then(
                response => {
                    
                    this.projects = response.data;
                    this.projects.forEach(project =>{
                        if (project.language !== null && ! this.skills.includes(project.language)) { 
                            this.skills.push(project.language)
                        };
                    });
                }
            )
            .catch(error=> {
                console.log(error);
                this.errors = true;
            })
            .finally(() => { 
                this.loading = false
                this.getProjects();
            })
        }, 
```
Let's break this down. First, we defined a method called `fetchData: function(){}` this method uses axios to make an api call to the
REST API. in the `.get()` method we have built the url also using the properties `perPage` and `page` as part of the query string.
the get method returns a promise so we used the `.then()` method on the promise to handle the response using an arrow function `response => {}`.

Inside the arrow function we stored the response data inside the projects property of the Vue instance using ` this.projects = response.data;`.
Next we used a `forEach` loop to iterate over each project and store the language used in the repository as a skills using the code below.
```js
this.projects.forEach(project =>{
    if (project.language !== null && ! this.skills.includes(project.language)) { 
        this.skills.push(project.language)
    };
});
```
we chained a `.catch` method to handle an error incase we are unable to connect to the rest api and fetch data, we will log the error to the
console and update the value of the `errors` property to true so that we can show a custom error message to the user later on. 

Finally, we chained the `.finally()` method that will be executed after the response has been handled and update the `loading` property and 
set it to false so that we can show the results to the user. Inside the finally method we can also call a method (that we still have to create)
and that we will use to slice the results later. 

Let's build it.
#### The Get projects method
This method takes a portion of the projects we actually stored in the `projects` property, we can use the `projectsList` property 
to store the slice and later implement a method to increment them with a show more button.
```js

getProjects: function(){

    this.projectsList = this.projects.slice(0, this.projectsCount);
    return this.projectsList;

},
```
The getProjects method takes a portion of all projects stored in the `projects` property using the array slice method in conjunction with
the property `projectsCount` that is set to five. So it will store in there only the first five results and return them.

To add five more projects to the `projectsList` property we will also need a method that the use can call when he clicks on the
load more button. Let's create it.
#### The load more projects method
The load more method will fist check if the length of the `projects` array is less or equal to the lenght of the `projectsList` array 
and, if not it will increment the value of the `projectsCount` property of five and then take a bigger slice from the `projects` property.

```js

loadMore(){
            
    if(this.projectsList.length <= this.projects.length){
        this.projectsCount += 5;
        this.projectsList = this.projects.slice(0, this.projectsCount)
    }
    

}
```

#### Build the template
In the template property of the projects component we can start with the header section, and 
put in there also two `router-link` components for the pages navigation.
```html
`<div>
        <header id="site_header" class="container d_flex">
            <div class="bio__media">
                <img src="./assets/img/avatar.svg" alt="">
                <div class="bio__media__text">
                    <h1>John Doe</h1>
                    <h3>Python Expert</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
                </div>
            </div>
            <nav>
                <router-link to='/'>Home</router-link>
                <router-link to="/projects">Project</router-link>
                <a href="https://">
                    <i class="fab fa-github fa-lg fa-fw"></i>
                </a>
            </nav>
        </header>

</div>

```
Next we can continue working on the template and create the main section.
We will put the following markup always in the main template div, right under the header closing
tag.

Let's start by placin the main container
```html
<main class="container">
    <!-- Show an error message if the REST API doensn't work -->
    <!-- Otherwise show  a section for our portfolio projects and skills section-->
</main>
```

Inside the container let's use the v-if-else directives to show an error message or
the projects section

```html
 <!-- Show Errors if the rest api doesn't work -->
    <div class="error" v-if="errors"> 
        Sorry! It seems we can't fetch data righ now 😥
    </div>
    <!-- Else show the portfolio section -->
    <section id="portfolio" v-else></section>
```
To make the code work we used the v-if directive and passed to it the `errors` property that will be 
set to `true`, if there is an error while we fetch data from gitHub or, will be set to `false` if 
everything is ok and, so the v-else directive will to render the portfolio section.

Next we need to show a 'loading...' message while we fetch data, and when done we can use the 
v-for directive to loop over the results. So right in the portfolio section we will write another
v-if-else diective.

```html
<section id="portfolio" v-else>
 <!-- Use a v-if directive to show the loading message -->
        <div class="loading" v-if="loading">😴 Loading ... </div>

        <!-- use a v-for directive to loop over the projectsList array -->
        <div v-for="project in projectsList" class="card__custom" v-else></div>

</section>
```
Here we use the v-if directive `<div class="loading" v-if="loading">😴 Loading ... </div>` to render a loading message, after that the `<div v-for="project in projectsList" class="card__custom" v-else></div>`
has two directives, the v-for directive that we use to loop over the `projectsList` property and a
v-else directive that will show this element when we are done fetching data from gitHub.

Now we can use the `project` variable to render all project details in our markup

```html
<!-- use a v-for directive to loop over the projectsList array -->
<div v-for="project in projectsList" class="card__custom" v-else>
    <div class="card__custom__text">
        <div>
            <!-- Create a custom method to trim the project name so that it doesn't break the design -->
            <h3>{{project.name}}</h3>
            <!-- Create a custom trimmedText to trim the description -->
            <p>{{project.description}}</p>                        
        </div>

        <div class="meta__data d_flex">
            <div class="date">
                <h5>Updated at</h5>
                <div>{{new Date(project.updated_at).toDateString()}}</div>
            </div>
            <img class="avatar" :src="project.owner.avatar_url">

        </div>
    </div>
    <div class="card__custom__img"></div>
    <div class="card_custom__button">
        <a :href="project.html_url" target="_blank">
            Code
        </a>
    </div>

</div>

```
To render the project title and desciption we used the propeties `poject.name` and `project.description`, however the description and the title will break our design unless we trim them at some
point. Next in the element with class `date` we rendered the poject data in a readable fomat using the ` new Data().toDateString()` method. To render the user avatar `<img class="avatar" :src="project.owner.avatar_url">` we used the shortcut for the v-bind diective so that we could use the property `project.owner.avatar_url` to grab the avatar url. Finally, to render a button that once clicked redirects
the user to the repository page we bound the `href` attribute to the `project.html_url` property  
`<a :href="project.html_url" target="_blank">Code</a>`.

Our project card is complete, the next thing we need to do is render a load more button 
to show more projects. 

We are still working inside the `projects` section, right after the poject card we can write the
following markup

```html
<!-- Render a load more button -->
<div style="text-align: center; width:100%" v-if="!loading" >
    <div v-if="projectsList.length < projects.length">
        <button class="btn_load_more" v-on:click="loadMore()">Load More</button>
    </div>
    <div v-else>
        <a href="" target="_blank" rel="noopener noreferrer">Visit My GitHub</a>
    </div>

</div>

```
the v-if directive first checks if the loading property is set to false, if so another v-if directive
is used to check if the length of property `projectsList` is less than the length of the property
`projects` and if so it will show a button that uses a v-on directive to listen for clicks 
`<button class="btn_load_more" v-on:click="loadMore()">Load More</button>` and trigger a loadMore() method. Otherwise we show a link to the github account. 

After it we can show a list of skills related to all the projects

```html
<!-- Show a skills section -->
<div id="skills_section">
    <h2>Development Skills</h2>
    <ul class="skills">
        <!-- Loop over the skills property -->
        <li v-for="skill in skills">{{skill}}</li>
    </ul>
</div>

```
Our markup is complete, however we need to improve our code a little as the project title and 
its description are breaking our design! 
Let's create two methods one to trim the title and one for the description text.

#### The trimText and trimTitle methods
The `trimTitle` method will replace all `-` and `_` with a space, and restrict the amount of characters
to 12, the `trimText` method instead only reduces the amount of characters of description in excess 
of 100 characters. 

```js
trimTitle: function(text){
    let title = text.replaceAll("-", " ").replace("_", " ")
    if(title.length > 15) {
        return title.slice(0, 12) + ' ...'
    } return title;

},
trimText: function(text){
    //console.log(text.slice(0, 100));
    if(text.length > 100) {
        return text.slice(0, 100) + ' ...'
    } return text;
},

```

With this two methods now we can update the markup and use them to make sure nothing breaks the design

Let's update this two line that will be changed from this:
```html
<!-- Create a custom method to trim the project name so that it doesn't break the design -->
<h3>{{project.name}}</h3>
<!-- Create a custom trimmedText to trim the description -->
<p>{{project.description}}</p>   
```

To this:
```html
<h3>{{trimedTitle(project.name)}}</h3>
<p>{{trimedText(project.description)}}</p>   
```


Let's put eveything together, the final markup will be the following

```html
<div>
    <header id="site_header" class="container d_flex">
        <div class="bio__media">
            <img src="./assets/img/avatar.svg" alt="">
            <div class="bio__media__text">
                <h1>John Doe</h1>
                <h3>Python Expert</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
            </div>
        </div>
        <nav>
            <router-link to='/'>Home</router-link>
            <router-link to="/projects">Project</router-link>
            <a href="https://">
                <i class="fab fa-github fa-lg fa-fw"></i>
            </a>
        </nav>
    </header>

        <main class="container">
        <div class="error" v-if="errors"> 
            Sorry! It seems we can't fetch data righ now 😥
        </div>

        <section id="portfolio" v-else>
            <div class="loading" v-if="loading">😴 Loading ... </div>
            <div class="projects" v-else>
                    <div v-for="project in projectsList" class="card__custom" >
                    <div class="card__custom__text">
                        <div>
                            <h3>{{trimedTitle(project.name)}}</h3>
                            <p>{{trimedText(project.description)}}</p>                        
                        </div>
                
                        <div class="meta__data d_flex">
                            <div class="date">
                                <h5>Updated at</h5>
                                <div>{{new Date(project.updated_at).toDateString()}}</div>
                            </div>
                            <img class="avatar" :src="project.owner.avatar_url">
                
                        </div>
                    </div>
                    <div class="card__custom__img"></div>
                    <div class="card_custom__button">
                        <a :href="project.html_url" target="_blank">
                            Code
                        </a>
                    </div>
                
                
                </div>


                <div style="text-align: center; width:100%" v-if="!loading" >
                    <div v-if="projectsList.length < projects.length">
                        <button class="btn_load_more" v-on:click="loadMore()">Load More</button>
                    </div>
                    <div v-else>
                        <a href="" target="_blank" rel="noopener noreferrer">Visit My GitHub</a>
                    </div>

                </div>

                <div id="skills_section">
                    <h2>Development Skills</h2>
                    <ul class="skills">
                        <li v-for="skill in skills">{{skill}}</li>
                    </ul>
                </div>
            </div>
        </section>  
    </main>
</div>

```

There is one last thing to do, since fetching data from gitHub is very fast we don't really see the 
loading message, let's set a timeout and delay it of a few seconds, you can tune it as you like.

### The mouted lifecycle hook

```
 mounted(){  

        setTimeout(this.fetchData, 3000 );
        
    }
```
Inside the mouted lifecycle hook we used `setTimeout()` and called the fetchData method as its 
first parameter, then for the second parameter we specified that this method should be executed
after 3000 milliseconds (3seconds). 



## Let's see our final code all toghether
Index.html file looks like the following
```html

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vuefolio</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;300;400;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
        integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <link rel="stylesheet" href="./assets/css/style.css">
</head>

<body>

    <div id="app">

        <!-- Render the component for the corresponding route -->
        <router-view></router-view>

    </div>
    <footer> © Developed by <a href="https://fabiopacifici.com">Fabio Pacific</a> </footer>
    <!-- Axios -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <!-- VueJS development version -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <!-- Vue Router -->
    <script src="https://unpkg.com/vue-router@2.0.0/dist/vue-router.js"></script>
    <!-- Main Js file -->
    <script src="./assets/js/main.js"></script>
</body>

</html>

```

And this is the main.js file
```js
// Create route components
const Home = {
    template: 
    `<main id="home">
        <div class="about__me">
            <img src="./assets/img/avatar.svg" alt="">
            <h1>John Doe</h1>
            <h3>Python Expert</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
    
            <div class="skills_projects_link"><router-link to="/projects">Projects/Skills</router-link> </div>
        </div>
    </main>`
}
const Projects = {
    template: 
    `<div>
        <header id="site_header" class="container d_flex">
            <div class="bio__media">
                <img src="./assets/img/avatar.svg" alt="">
                <div class="bio__media__text">
                    <h1>John Doe</h1>
                    <h3>Python Expert</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
                </div>
            </div>
            <nav>
                <router-link to='/'>Home</router-link>
                <router-link to="/projects">Project</router-link>
                <a href="https://">
                    <i class="fab fa-github fa-lg fa-fw"></i>
                </a>
            </nav>
        </header>
    
         <main class="container">
            <div class="error" v-if="errors"> 
                Sorry! It seems we can't fetch data righ now 😥
            </div>

            <section id="portfolio" v-else>
                <div class="loading" v-if="loading">😴 Loading ... </div>
                <div class="projects" v-else>
                     <div v-for="project in projectsList" class="card__custom" >
                        <div class="card__custom__text">
                            <div>
                                <h3>{{trimedTitle(project.name)}}</h3>
                                <p>{{trimedText(project.description)}}</p>                        
                            </div>
                    
                            <div class="meta__data d_flex">
                                <div class="date">
                                    <h5>Updated at</h5>
                                    <div>{{new Date(project.updated_at).toDateString()}}</div>
                                </div>
                                <img class="avatar" :src="project.owner.avatar_url">
                    
                            </div>
                        </div>
                        <div class="card__custom__img"></div>
                        <div class="card_custom__button">
                            <a :href="project.html_url" target="_blank">
                                Code
                            </a>
                        </div>
                    
                    
                    </div>


                    <div style="text-align: center; width:100%" v-if="!loading" >
                        <div v-if="projectsList.length < projects.length">
                            <button class="btn_load_more" v-on:click="loadMore()">Load More</button>
                        </div>
                        <div v-else>
                            <a href="" target="_blank" rel="noopener noreferrer">Visit My GitHub</a>
                        </div>

                    </div>

                    <div id="skills_section">
                        <h2>Development Skills</h2>
                        <ul class="skills">
                            <li v-for="skill in skills">{{skill}}</li>
                        </ul>
                    </div>
                </div>
            </section>
        
           
        </main>
    </div>`,
data() { 
    return {
        data: [],
        projects: [],
        projectsList: null,
        skills: [],
        projectsCount: 5,
        perPage: 20,
        page: 1,
        loading: true,
        errors: false,
        }
    },
    methods: {
        trimedTitle: function(text){
            let title = text.replaceAll("-", " ").replace("_", " ")
            if(title.length > 15) {
                return title.slice(0, 12) + ' ...'
            } return title;
        
        },
        trimedText: function(text){
            //console.log(text.slice(0, 100));
            if(text === null) {
                return 'This project has no description yet!';
            } else if(text.length > 100) {
                return text.slice(0, 100) + ' ...'
            } 
            return text;
        
        },
        getProjects: function(){

            this.projectsList = this.projects.slice(0, this.projectsCount);
            return this.projectsList;
        
        },
        fetchData: function(){
            axios
            .get(`https://api.github.com/users/fbhood/repos?per_page=${this.perPage}&page=${this.page}`)
            .then(
                response => {
                    this.projects = response.data;
                    this.projects.forEach(project =>{
                        if (project.language !== null && ! this.skills.includes(project.language)) { 
                            this.skills.push(project.language)
                        };
                    });
                }
            )
            .catch(error=> {
                console.log(error);
                this.errors = true;
            })
            .finally(() => { 
                this.loading = false
                this.getProjects();
            })
        }, 
        loadMore(){
            
            if(this.projectsList.length <= this.projects.length){
                this.projectsCount += 5;
                this.projectsList = this.projects.slice(0, this.projectsCount)
            }
            
        
        }
        
    },
    mounted(){  

        setTimeout(this.fetchData, 3000 );
        
    }
}

// Define routes
const routes = [
    {path: '/', component: Home},
    {path: '/projects', component: Projects},
];


// create the router instance
const router = new VueRouter({
    routes
})

// create and mount the vue instance
const app = new Vue({
    router
}).$mount('#app');

```
On the CSS Side instead this is what we have
```css

/* Utility Classes */
.d_none {
    display: none;
}
.d_flex {
    display: flex;
}
.container {
    max-width: 1170px;
    margin: auto;
}
a {
    color: white;
} 
a:hover {
    color:#DB5461;
    
}
.loading {
    font-size: 2rem;
}
/* END Utility Classes */
/* Components */
.bio__media {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    text-align: left;
}
.bio__media img {
    height: 120px;
}
.bio__media__text {
    padding: 1rem;
}
.bio__media__text h1{
    font-size: 36px;
    font-weight: 900;
    color: #DB5461;

}
.bio__media__text p {
    font-weight: 100;
    font-size: 16px;
    line-height: 1.5rem;
    
}

.card__custom {
    position: relative;
    display: flex;
    max-width: 400px;
    height: 300px;
    min-height: 300px;
    padding: 0.5rem;
    margin-bottom: 3rem;
    flex-grow: 1;
    flex-basis: calc(100% /2);
    align-items: center;
    justify-content: space-between;
}
.card__custom > .card__custom__text {
    max-width: calc((100% / 3) *2);
    text-align: right;
    height: 80%;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    overflow: hidden;

}
.card__custom__img {
    
    position: absolute;
    width: 70%;
    height: 100%;
    background-image: url(../img/cards_bg_img.svg);
    background-position: center;
    background-repeat: no-repeat;
    background-size: contain;
    display: inline-block;
    z-index: -1;
    left: 60%;
    transform: translateX(-50%);
    border-radius: 85px 0 100px 25px;

}
.card_custom__button a, .btn_load_more {
    background: #F1EDEE;
    border: 5px solid #3D5467;
    box-sizing: border-box;
    border-radius: 54px;
    padding: 0.5rem 1rem;
    font-weight: 900;
    color: #3D5467;
}
.card_custom__button a:hover, .btn_load_more:hover {
    cursor: pointer;
    background: #324555;
    color: white;
    border-color: #DB5461;
    transition: 1s;
}
.card__custom__text h3 {
    text-transform: uppercase;
    font-size: 1.5rem;
}
/* END Componenet */
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

body{
    font-family: 'Raleway', Arial, Helvetica, sans-serif;
    color: white;
    background: linear-gradient(116.82deg, #3D5467 0%, #1A232B 99.99%, #333333 100%);

}
a {
 text-decoration: none;   
}

/* Home Page */
main#home {
    width: 100%;
    height: 100vh;
    min-height: 600px;
    display: flex;
    justify-content: center;
    align-items: center;
}
#home > .about__me {
    text-align: center;
    width: 80%;
    line-height: 1.5rem;
}
#home > .about__me > h1 {
    margin: 20px 0 0;
    font-size: 36px;
    font-weight: 900;
    color: #DB5461;
}
#home > .about__me > h3 {
    font-size: 28px;
    font-weight: 500;

}
#home > .about__me > h1, #home > .about__me > h3  {
    font-style: normal;
    line-height: 42px;
    letter-spacing: 0.115em;

}
#home > .about__me p {
    font-weight: 100;
    font-size: 22px;
    padding: 2rem;
}
.skills_projects_link {
    position: relative;
}
.skills_projects_link > a {
    text-transform: uppercase;
    color: white;
    font-weight: 900;
    font-size: 18px;
    line-height: 21px;

}
.skills_projects_link > a:hover {
    color: #DB5461;
    transition: all 0.5s ease-in-out;

}
.skills_projects_link > a:hover::after {
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
    display: flex;
    margin: auto;
    text-align: center;
    content: "";
    width: 30px;
    height: 2px;
    background-color: #DB5461;
    transition: background-color 0.5s ease-in-out;

}

/* Header */
#site_header {
    text-align: center;
    padding: 2rem 0;
    justify-content: space-between;
    align-items: center;
}
#site_header > h1 {
    text-transform: uppercase;
}
nav a {
        color: #e2e2e2;
    text-transform: uppercase;
    font-weight: 900;
}
nav a:hover {
    color: #DB5461;
}
/* Portfolio Page Section */

#portfolio {
    margin-top: 4rem;
    
}


#portfolio .projects {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: space-around;
}
/* Skills */

#skills_section {
    margin-top: 4rem;
    min-height: 300px;
    background-image: url(../img/skills_bg.svg);
    background-repeat: no-repeat;
    background-size: contain;
    background-position: top left;
}
#skills_section h2 {
    margin-left: 180px;
    font-size: 44px;
    color: #F1EDEE;
    line-height: 2rem;

}
#skills_section ul {
    list-style: none;
    margin: 20px 120px;
    display: flex;
    flex-wrap: wrap;

}
#skills_section ul  li {
    padding: 1rem;
    margin: 0.5rem;
    background-color: #DB5461;
    border: 5px solid #3D5467;
    border-radius: 35px;
}



.avatar {
    width: 30px;
    height: 30px;
    border-radius: 50%;
        margin: 0 1rem;

}


.card__back {
    display: none;
}
.rotate__card {
    transform: rotate3d(360,0,0,180deg);
}
/* Site Footer */

footer {
    text-align: center;
    padding: 2rem 0;
}



/* Media Query  */

@media screen and (max-width: 475px) {
    .card {
        flex-basis: 100%;
        width: 100%;
    }
}

```

That's it! We are ready to deply our code to production!



## Next!
In an upcoming serie I'll show you how to test you code, upgrade to vue3 and more. 
If you enjoyed this video serie hit the like button and enable notifications, just click bell to know when my next video is online. If you have any question leave a comment, I reply to all YouTube comments.

