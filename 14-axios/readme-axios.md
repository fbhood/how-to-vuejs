## API calls with Axios
Watch the video on [YouTube](https://youtu.be/XJEmPr89HA8)
Check the repository on [BitBucket](https://bitbucket.org/fbhood/how-to-vuejs/src/master/14-axios/)

For our next project I have created a simple but nice design using figma that we will use to kick start our portfolio.
Our portfolio will use the GitHub rest API to pull projects and fill up the design. 

To quote Wikipedia:
"A REST API is a software architectural style that enables the requesting system to access and manipulate a textual representation of web resources."

What this means is that our Vue application (the requesting system) will request a textual representation from GitHub of our repositories that later we can use and manipulate to showcase our projects inside our portfolio.

For our final project we will use a library called axios that will help us to make HTTP requests to the GiHub API. 
We can install axios inside our project in multiple ways, for our example we will keep things simple and, use the CDN. 

There are also other methods you can use to install Axios. 
The official documentation for Axios is available [here](https://www.npmjs.com/package/axios)
Consume API, [documentation](https://vuejs.org/v2/cookbook/using-axios-to-consume-apis.html#Base-Example)

### Installation via CDN
So let's get started and install axios via the CDN. We will use the UNPKG CDN and, insert a script tag inside our main HTML file. 
This CDN will provide always the most up to date version of axios, alternatively we can also specify a different version number. 

Let's start by inserting the following script in an index.html file that we will use to send our first HTTP request to the GitHub API.


```html
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
```

Our final HTML file will look like this now:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VueJS / GitHub API</title>
    

</head>
<body>
    <div id="app"> </div>

    <!-- Axios latest version CDN -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <!-- VueJS development version -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="./main.js"></script>
</body>
</html>

```
The code above is nothing new but, let's look at it piece by piece.
We have a basic HTML stricture, bofore the closing body tag we placed two script tags, one for Axios and one for VueJs. 
In the body we created a root element for the Vue application that we called `#app`.
Finally before the end of the body tag we placed a new script tag that points to the file 
where we will write our code, the main.js file.

Now we have all the building blocks to make our first API call and request data from the GitHub API.
But before that let's quickly see what is an HTTP request and, what kind of requests we can make.

### What is an HTTP and what are HTTP requests
HTTP stands for Hypertext transfer protocol, it is an application-layer protocol designed for communications between two points:
1. a web client (the browser) 
2. a web server. 

This protocol allows transmission of data like HTML files and, it defines verbs also known as methods that can be used to perform specific actions on a given resource.

The method or verb that we will use for our project is the `GET` method, that as you might have guessed, is used to obtain or to get 
a resource from the web server.

We have also other methods:
- GET (retrieves data)
- POST (sends data)
- PUT (updates the entire representation of the data)
- PATCH (similar to put but used to partially update data)
- DELETE (removes data)

Each of these requests as I said earlier performs a specific action on a resource, but there are also other verbs like the HEAD, OPTIONS, CONNECT, TRACE.

I won't cover HTTP in detail here as it's out to the scope of this guide but, below there are a bounch of link to documentation pages 
related to this topic if you want to find our more. 

I suggest you to read the following at least:
- [HTTP Intro](https://developer.mozilla.org/en-US/docs/Web/HTTP)
- [HTTP Overview](https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview)
- [HTTP Methods](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods)


### Perform a GET request
We will use the GET method to perform get requests to the GitHub API. All data we want to request are publicly accessible (the public repositories of a user), therefore we don't have to authenticate our application. 
However, unauthenticated requests are limited, but for the scope of this tutorial this is perfectly fine. If you plan to put this in 
production then you might want to look at how to make authenticated requests and obtain an api key from gitHub.

GitHub provides a clear and, in-dept documentation about its Rest API, including a list of resources that can be requested and their endpoints. We will use the "List repositories for a user" resources available [here](https://docs.github.com/en/free-pro-team@latest/rest/reference/repos#list-repositories-for-a-user).


Let's look at the documentation.
The first thing we notice is that GitHub gives us a GET endpoint where we can send our HTTP requests `/users/{username}/repos` 
the placeholder `{username}` needs to be replaced with actual username of the user we want to request the list of public repositories from.

From the documentation we also see that there are other parameters that we can use to refine our request. 
We will use `username` that goes in the path and needs to be a string as described in the parameters table under Type. 
We can also use the `per_page` and `page` parameters to paginate our results.


Let's make a first request and see what we get.
Inside our main.js file we will create a new vue instance and add a `mounted` lifecycle hook 
where we will perform the HTTP request using Axios.

```js
let app = new Vue({
    el:'#app',
    data:{
        projects: [],
        perPage: 20,
        page: 1
    },
    mounted(){
        
         axios
         .get(`https://api.github.com/users/fabiopacifici/repos?per_page=${this.perPage}&page=${this.page}`)
         .then(
            response => {
                console.log(response);
                this.projects = response.data;
            }
        )
        .catch(error=> {console.log(error);})
    }
});
```

Let's break this code down. 
First we have created a new Vue instance. Then used the `el` property and assigned it a root HTML element. 

Then we have defined a `data` object and in here defined thee properties that we will use later to perform the HTTP request and handle the response.

After the data object we have defined a lifecycle hook that will use to run our code once the root element has been mounted.

Inside the mounted method it's time to use Axios and perform an HTTP request. 
Axios is a promise based HTTP client, when we use the get method to request our data from the GitHub API it will return a promise that needs to be handled. 
This is done using the syntax `axios.get()` to perform the request, than we handle its response using the `.then()` method on the promise. 
If our request fails the `.catch()` method will handle the error and, in this case show the error message on the console.

Promises are out of the scope of this guide but, if you want to learn more, you can visit the documentation [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises).


Inside the `.get()` method we have put the URL including a query string that uses `per_page` and `page` parameters to submit our request, inside the `.then()` method we handled the response, the response parameter is given us by the promise and, we use an arrow function to handle it.
```js 
response => {
                console.log(response);
                this.projects = response.data;
            }
```
The get method returns a promise, here we simply handled its `response` with an arrow function where response is the return value that we obtained by calling `axios.get()`.
We logged to the console the response object. Then we assigned its content, the `response.data`, to our `projects` property so 
that we can later retrieve each project and, show them onto the page as usual with a `v-for` directive.

### Show each project 
Now it's time to show our projects inside the portfolio and we do that with the v-for directive.
The projects property in this case contains an array of objects each object has its properties that we can use to populate our template.

```html
<div id='app'>

    <div v-for="project in projects">
        <h2 class="title">{{project.full_name}}</h2>
        
        <div class="author">
            <img width="50px" :src="project.owner.avatar_url" alt="me">
        </div>
        <div class="view">
            <a :href="project.html_url">View</a>
        </div>
    </div>

</div>
```
Here we use the v-for directive to loop over the array of projects.
Now the `project` variable contains an object that represents a single repository from the GitHub account.

Looking at the response object we know that we can grab a number of properties, so we picked `full_name`, the full name for the repository, `owner.avatar_url` the url of the profile's avatar, then `html_url` that is the actual url of our repository. That's all we need for now. 


If we now look at the page we will immediately see all repositories from our account.
Now that we know how to make an HTTP request with Axios and get data from GitHub we are almost ready to start building our portfolio. 

In the next video we are goint to look at another Vue library called Vue-router that we will use in our final project.

Until the next time, remember to like the video, [subscribe](https://www.youtube.com/channel/UCTuFYi0pTsR9tOaO4qjV_pQ) to my channel and, leave a comment if you have any question. 

Stay safe guys, bye!