let app = new Vue({
    el: '#app',
    data: {
        tweet: "",
        tweets: [],
        max_length: 200        
    }, 
    computed: {
       /*  maxCharText: function () {
           return ` Max: ${this.tweet.length} of ${this.max_length} characters` 
        },
        errorMessage: function () {
            return `Max char limit reached! excess characters: ${this.max_length -
                this.tweet.length} `
        } */
    },
    methods: {
        submitData(){
            // Handle the tweet submission
            if(this.tweet.length <= this.max_length){
                this.tweets.unshift(this.tweet);
                this.tweet = "";
            }
        },
        maxCharText: function () {
           return ` Max: ${this.tweet.length} of ${this.max_length} characters` 
        },
        errorMessage: function () {
            return `Max char limit reached! excess characters: ${this.max_length -
                this.tweet.length} `
        }
    }
})