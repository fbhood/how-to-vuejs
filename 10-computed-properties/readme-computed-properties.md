
## Computed Properties and methods
View the [Repository](https://bitbucket.org/fbhood/how-to-vuejs/src/master/10-computed-properties/) | Watch the video on [YouTube](https://youtu.be/VxFT6cgTHhw)

A computed property should be used over in-template expressions for complex logic that has the scope of changing the presentation of our data not the data it-self. If we need to change the data then methods should be used instead. Computed properties are cached based on their dependencies meaning that it will re-evaluate only when its dependencies have changed. 
With computed properties the result of the previously run function is returned if the dependencies have not changed. 

In the following example we will use computed property but we could also use methods. Generally speaking we use computed properties when we have an expensive operation that we want to execute and cache so that the next time we don't have to run it again unless something has 
changed.  

Let's implement a computed property for the following messages
Our HTML file will now change from this

```html
<!-- Show the max char messages -->
<div>
    <span v-if="tweet.length < max_length"> {{ `Max: ${tweet.length} of ${max_length} characters` }}
    </span>
    <span class="errorMessage" v-else>{{`Max char limit reached! excess chars: ${max_length - tweet.length}`}}</span>

</div>

```

to this much clean version:

```html
<!-- Show the max char messages -->
<div>
    <span v-if="tweet.length < max_length"> {{ maxCharsText }}
    </span>
    <span class="errorMessage" v-else>{{errorMessage}}</span>
    
</div>

```
We have replaced the contents of both span with two new properties that will be placed as methods inside our computed object.

Now inside our Vue instance we will create a new object called `computed` where we will define two methods that will return the messages we had before. 

```js
let app = new Vue({
    el: '#app',
    data: {
        tweets: [],
        tweet: "",
        max_length: 200,  
        error: ""
    },
    // Computed Properties
 computed: {
        maxCharsText: function(){
            return `Max: ${this.tweet.length} of ${this.max_length} characters`;
        },
        errorMessage: function(){
            return `Max char limit reached! excess chars: ${this.max_length - this.tweet.length}`
        }
    },
    // Methods
 methods: {
          submitData(){
              if (this.tweet.length <= this.max_length) {
                  this.tweets.unshift(this.tweet);
                  this.tweet = "";
              } 
        }
    },

});

```
The first method `maxCharsText` returns exactly the same string we had before inside our HTML file, the only difference is that we are using the keyword `this` to reference to the properties we needed to grab inside the Vue instance `this.tweet.length` and `this.max_length`. 
The second method works in the exact same way and it also uses the keyword `this` to pick the properties defined in the Vue instance `this.max_length` and `this.tweet.length`.


### All together
```html
<div id="app">
    <h2>What do you want to tweet about today?</h2>
    <form v-on:submit.prevent="submitData">
        <div class="form_group">
            <label for="name">Tweet</label>
            <textarea name="tweet" id="tweet" cols="80" rows="10" v-model="tweet"></textarea>

        </div>

        <button type="submit">Next</button>

    </form>

    <div>
        <span v-if="tweet.length < max_length"> {{ maxCharsText }}
        </span>
        <span class="errorMessage" v-else>{{errorMessage}}</span>
       
    </div>
    <ul>
        <li v-for="text in tweets">{{text}}</li>
    </ul>
</div>

```
JavaScript file

```js
let app = new Vue({
    el: '#app',
    data: {
        tweets: [],
        tweet: "",
        max_length: 200,  
        error: ""
    },
    // Computed Properties
 computed: {
        maxCharsText: function(){
            return `Max: ${this.tweet.length} of ${this.max_length} characters`;
        },
        errorMessage: function(){
            return `Max char limit reached! excess chars: ${this.max_length - this.tweet.length}`
        }
    },
    // Methods
 methods: {
          submitData(){
              if (this.tweet.length <= this.max_length) {
                  this.tweets.unshift(this.tweet);
                  this.tweet = "";
              } 
        }
    },

});


```


If we want to use methods instead of computed properties, we can simply move both methods from the `computed` object inside the `methods` object and invoke them with parenthesis inside our HTML file like so:

```html
<div>
    <span v-if="tweet.length < max_length"> {{ maxCharsText() }}
    </span>
    <span class="errorMessage" v-else>{{errorMessage() }}</span>
    
</div>

```
Just remember that computed properties are cached while methods are not. 
If you want to learn more make sure to read the official [documentation](https://vuejs.org/v2/guide/computed.html).