
## Simple_twitter with components
Watch on [YouTube](https://youtu.be/HanHyGFC6Sc)
Checkout the repository [here](https://bitbucket.org/fbhood/how-to-vuejs/src/master/13-simple-twitter-components/)

Now that we have a basic understanding of components we can update the simple twitter project we built in the previous videos and use components to make our code better.

We weed to do a few things to make this happen, and create a component: 
 1. We need to decide what component we want to build 
 2. We need to extract the code from the markup and place it in the template property
 3. We need to refactor our code to make the component work.


Let's say we want to create a component for the tweet message. 
## First, we need to create the component:
```js
Vue.component('tweet-message',{
    template: ``
});
```
## Move the tweetMsg element:
Then we have to move the `tweetMsg` element inside the  `template` property of our component
```js

Vue.component('tweet-message',{
    template: `
    <div class="tweetMsg" v-for="(tweet, index) in tweets">
        <p>
            {{ tweet.text}}
        </p>
        <div class="tweetDate">
            <i class="fas fa-calendar fa-sm fa-fw"></i>{{ tweet.date }}
        </div>
        <div class="tweet_remove" @click="removeTweet(index)">
            <span class="remove">Delete this tweet <i class="fas fa-trash fa-sm fa-fw"></i></span>
        </div>
    </div>
    `
});

```
After that, we need to update the template because the v-for directive now is useless, so we will remove it and add it back later when we are ready to use the component.
Given that we will not have a v-for directive at this point we still want to use the tweet variable to grab the tweet so we will pass it as a `props`.


```js
Vue.component('tweet-message',{
    props: {
        'tweet': Object,
    },
    template: `
    <div class="tweetMsg">
        <p>
            {{ tweet.text}}
        </p>
        <div class="tweetDate">
            <i class="fas fa-calendar fa-sm fa-fw"></i>{{ tweet.date }}
        </div>
        <div class="tweet_remove" @click="removeTweet(index)">
            <span class="remove">Delete this tweet <i class="fas fa-trash fa-sm fa-fw"></i></span>
        </div>
    </div>
    `
});
```
## Emit a custom Event
There is also an event litenter that needs to change to let our application work as expected. 
```html
<div class="tweet_remove" @click="removeTweet(index)">
    <span class="remove">Delete this tweet <i class="fas fa-trash fa-sm fa-fw"></i></span>
</div>
```
The code here `<div class="tweet_remove" @click="removeTweet(index)">` listens to click events so the user can remove a tweet clicking on it. 
This will need to go and, we need to replace it with a special method of the Vue instance called $emit(). Our component instance will need to communicate with the parent instance and tell it that it wants to trigger remove tween method.  

To solve this problem Vue provides a custom events system. It allows us to use the v-on directive to liten not only to native DOM events but also to custom events defined at component level.

We need to update this line of code: 
```html
<div class="tweet_remove" @click="removeTweet(index)">

```
and change it like so: 
```html
<div class="tweet_remove" @click="$emit('remove-tweet', 'index')">
```
Let's break this down, we keep using the v-on directive in its short form `@` then we 
use the Vue `$emit` method to define a custom event that our component will emit when we 
click on this element. To the `$emit` method we pass two parameters, the first 
is the name of the custom event `remove-tweet`, the second is a parameter that we want to pass to the event litener when used `index` that will be the index of the element we want to delete, so that the parent instance can listen to our event, trigger the `removeTweet` method we defined in the main Vue instance and remove the correct tweet.


## Put all togheter
Our final component now looks like that:
```js

Vue.component('tweet-message',{
    props: {
        'tweet': Object,
    },
    template: `
    <div class="tweetMsg">
        <p>
            {{tweet.text}}
        </p>

        <div class="tweetDate">
            <i class="fas fa-calendar-alt fa-sm fa-fw"></i>{{tweet.date}}
        </div>
        <div class="tweet_remove" @click="$emit('remove-tweet', 'index')">
            <span class="remove">Delete this tweet <i class="fas fa-trash fa-xs fa-fw"></i></span>
        </div>
        
    </div>
    `
});

```
Our index.html file will be changed as follow 

```html
<!-- Show all tweets -->
<div class="card_tweets">
    <section class="tweets" v-if="tweets.length > 0">
        <h2>Tweets</h2>
        <tweet-message v-for="(tweet, index) in tweets"  v-bind:tweet="tweet" :key="index" @remove-tweet="removeTweet(index)"></tweet-message>
    </section>
    <div v-else>No tweets to show</div>
</div>
```
Now that our first project is completed let's learn how to make an API request and how to use the GitHub API to build our final portfolio.

Before the next time, remember to like the video, subscribe, leave a comment if you have any question and, see you later.


