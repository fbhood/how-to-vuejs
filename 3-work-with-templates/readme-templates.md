## Work with Templates
Click to view the [Repository](https://bitbucket.org/fbhood/how-to-vuejs/src/master/3-work-with-templates/)

Click to view the [YouTube-Video](https://youtu.be/pDj3SQ8TNzs)

VueJS uses the mustache syntax {{ }} to render data from the Vue instance inside the HTML element. 
Using that we can grab properties and methods defined in the Vue instance. 
The property is then parsed and rendered to the page.

### Text Data binding
This is called text data bining, let's see an example of how we can bind data between the Vue instance and our template file. 

```html

<div id="app">
    <h1>{{ title }}</h1>
</div>
```
The code above has an `h1` tag inside the root element with an id of `app` we defined in the previous chapter. Inside the `h1` tag we use the double curly brackets syntax to render onto the page the value of a property in the data object called that we called `title`. 
We don't have a `title` property yet inside our data object, so let's add it.

Inside the main.js file
```js

let app = new Vue({
    el: "#app",
    data: {
        title: "John Doe portfolio",
        projects: [
            {title: "portfolio", languages: ["HTML", "CSS", "VueJS"]},
            {title: "grocery shop", languages: ["HTML", "CSS", "PHP"]},
            {title: "blog", languages: ["HTML", "CSS", "PHP"]},
            {title: "automation script", languages: ["Python"]},
            {title: "eCommerce", languages: ["HTML", "CSS", "PHP"]},
        ]
    }
})

```

Now, with the code above we can render the content of the property `title` inside the `h1` tag in our template. The final result will be something like that: 

```html
<h1>John Doe portfolio</h1>
```

However, with this method we can pass only a string, if we want to use HTML tags inside the string 
these will not be parsed but shown as simple strings.

For instance if we assign the following string to the `title` property
```js
    title: "John Doe <span class='badge'>Portfolio</span>"
```
And then try to render it inside our HTML like so:
```html
<h1 class="title">{{title}}</h1>
```
The property `title` will be rendered as a plain string including the HTML tags 
ie. ```John Doe <span class='badge'>Portfolio</span>```

Obviously we can parse html too.
### Parse raw HTML
To render a raw HTML element we need to introduce another concept of Vue, called directives. 
In this case we will use the v-html directive inside our HTML tag as an attribute and, 
pass to it the property title. 
When using Vue directives the text inside quotes is considered a JavaScript expression 
therefore it's computed and, its result rendered.  

Let's create a separate property for the title with HTML tags inside so that we can see 
how both render onto the page. 

```js
let app = new Vue({
    el: "#app",
    data: {
        title: "John Doe Portfolio", 
        titleHTLM : "John Doe <span class='badge'>Portfolio</span>",
        projects: [
            {title: "portfolio", languages: ["HTML", "CSS", "VueJS"]},
            {title: "grocery shop", languages: ["HTML", "CSS", "PHP"]},
            {title: "blog", languages: ["HTML", "CSS", "PHP"]},
            {title: "automation script", languages: ["Python"]},
            {title: "eCommerce", languages: ["HTML", "CSS", "PHP"]},
        ]
    }
})

```
Now inside our HTML file we will use this `{{}}` syntax to render the property `title` but, 
on the tag where we want to render raw HTML, with the `titleHTML` property, we use the v-html 
directive instead.

```html
<div id="app">
    <div class="title">{{ title }}</div>
    <div v-html="titleHTML"></div>
</div>
```
Both elements will now render correctly including the second property that has HTML tags inside.
NOTE: Render HTML can expose XSS vulnerabilities. Never use this approach on user-provided contents.

Now that we know how to render data onto the page let's dig deeper into directives. 
### Attributes and the v-bind directive
We have used the double curly brace syntax to render something between HTML opening and closing tags, but inside an HTML tag we cannot use the same syntax. To connect an HTML attribute to the Vue instance we use the `v-bind` directive. 
It lets you access properties in the data object but without curly braces.
The v-bind directive is one of the directives that takes arguments, they are specified after the colon. 
In our case, what we specify after the colon is the name of the HTML attribute we want to bind, like `v-bind:id`, `v-bind:class`, `v-bing:href`, `v-bind:src` etc. 

So, if we need to dynamically assign an attribute like an `href` or a `class` we can bind it with the Vue instance using the `v-bind` directive.
Let's see v-bind in action and start by connecting `id` and `class` attributes to the Vue instance.

Inside our index.html file
```html
<div id="app">
    <div 
    v-bind:class="dynamicClass" 
    v-bind:id="dynamicId">
        Dinamically assign a class and an id to the div
    </div>
</div>
```
Let's break the code above and see what it's doing. 
First we have a `div` tag inside the root element, then we use the `v-bind:class` and the `v-bing:id` directives of the class and id attributes. 
Inside quotes we specify two properties that later we will define in the data object of the Vue instance.
REMEMBER: When using Vue directives the content between quotes is treated as a JavaScript expression.

Let's define these two properties inside the Vue instance.
```js
let app = new Vue({
    el: "#app",
    data: {
        title: "John Doe Portfolio", 
        titleHTLM : "John Doe <span class='badge'>Portfolio</span>",
        projects: [
            {title: "portfolio", languages: ["HTML", "CSS", "VueJS"]},
            {title: "grocery shop", languages: ["HTML", "CSS", "PHP"]},
            {title: "blog", languages: ["HTML", "CSS", "PHP"]},
            {title: "automation script", languages: ["Python"]},
            {title: "eCommerce", languages: ["HTML", "CSS", "PHP"]},
        ],
        dynamicId : "projects_section",
        dynamicClass : "projects"
    }
})


```
We have defined `dinamicId: "projects_section"` and `dynmicClass: "projects"` properties and assigned values to them.

Thanks to the data binding on the attributes our HTML tag will be rendered our values dynamically and we can also dynamically change our attributes values.

```html
<div id="projects_section" class="projects">Dynamically assign a class and an id to the div</div>
```

### V-bind with Boolean values
With attributes using a boolean value, the v-bind directive works differently. It shows the attribute only if the property's value is true. In all other cases it won't render the attribute and its content at all. 

For the next example we will use a button with the disabled attribute.

Inside our root HTML element:
```html
    <div id="app">
        <button v-bind:disabled="disabled">You can't click this button</button>
    </div>
```
Inside the Vue instance we write the following code:
```js
let app = new Vue({
    el: '#app',
    data: {
    //disabled: false, // wont render the attribute
    //disabled: null, // wont render the attribute
    //disabled: undefined, // wont render the attribute
    disabled: true // renders the attribute
}
})

```
Only when the disabled property is set to true the attribute becomes visible and renders onto the page.

```html
    <button disabled>You can't click this button</button>
```
This is something to keep in mind when working with such attributes.

Another thing to consider is that bindings can include a single JavaScript expressions, with some restrictions:
- only expressions are allowed
- only a single expression
- no statements
- no flow control tools, but the ternary operator works.

If you want to read more, visit the documentation 
here:[https://vuejs.org/v2/guide/syntax.html#Using-JavaScript-Expressions]
