
// Create route components
const Home = {template: `<div>
<h1>My Portfolio</h1>

</div>`}
const Projects = {template: `
<div>
<h1>My projects</h1>
<div v-for="project in projects">
    <h2 class="title"> {{ project.full_name }}</h2>
    <div class="author">
        <img width="50" :src="project.owner.avatar_url" alt="me">
    </div>
    <div class="view">
        <a :href="project.html_url">View</a>
    </div>
</div>
</div>
`,
data() { 
    return {
        projects: [],
        perPage: 20,
        page: 1
        }
    },
    mounted(){
        // Api call

        axios.get(`https://api.github.com/users/fbhood/repos?per_page=${this.perPage}&page=${this.page}`)
        .then(response => { 
            console.log(response);
            this.projects = response.data;
        }).catch(error => { 
            console.log(error);
        })

    }
}

// Define routes

const routes = [
    {path: '/', component: Home},
    {path: '/projects', component: Projects},
];


// create the router instance
const router = new VueRouter({
    routes
})

// create and mount the vue instance

const app = new Vue({
    router
}).$mount('#app')


/* const app = new Vue({
    el: '#app',
    data: {
        projects: [],
        perPage: 20,
        page: 1
    },
    mounted(){
        // Api call

        axios.get(`https://api.github.com/users/fbhood/repos?per_page=${this.perPage}&page=${this.page}`)
        .then(response => { 
            console.log(response);
            this.projects = response.data;
        }).catch(error => { 
            console.log(error);
        })

    }
}) */