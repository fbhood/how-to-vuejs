
## Routing with VueRouter
Watch on [YouTube](https://youtu.be/T_avTRFAEAg)
Checkout the repository on [BitBucket](https://bitbucket.org/fbhood/how-to-vuejs/src/master/15-routing/)
Our portfolio will surely have more than one page, therefore we need a system that understands where to send the user when for instance a link in the navbar is clicked for a specific page. 
For that Vue has an official routing package that can help us doing just that and build a single page application.
A single page application is an application that doesn't refresh the page when a new page is visited so 
that the user experience is more fluid and nice.

Like for Vue and Axios we need to install this library and, we do that via its CDN. But as always, there are also other methods depending on your needs but we want to keep things simple for now so let's start by placing the CDN script tag inside the HTML file and learn the basics of this new library.

Vue Router Documentation [here](https://router.vuejs.org/installation.html#direct-download-cdn).
### Install via CDN
Let's take our previous example index.html and after the VueJS CDN will point to the router `https://unpkg.com/vue-router@3.4.9/dist/vue-router.js`.

```html

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VueJS / GitHub API</title>
  
</head>
<body>
    <div id="app"> 
        <div v-for="project in projects">
            <h2 class="title">{{project.full_name}}</h2>
            
            <div class="author">
                <img width="50px" :src="project.owner.avatar_url" alt="me">
            </div>
            <div class="view">
                <a :href="project.html_url">View</a>
            </div>
        </div>

    </div>
      <!-- Axios latest version CDN -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <!-- VueJS development version -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    
    <!-- Vue Router CDN -->
    <script src="https://unpkg.com/vue-router@3.4.9/dist/vue-router.js"></script>
    
    <!-- Main scrip file -->
    <script src="./main.js"></script>
</body>
</html>
```
### How to use Vue Router
Now our app has access to the router system and we can add a couple of routes for our application.
We can do so using the `router-link` component provided by the library and its `to` attribute to point the link to a specific page.
```html
<!-- Create a router link using the 'router-link' component and set the path using the 'to' attribute -->
<header>
    <nav>
        <router-link to="/">Home</router-link>
        <router-link to="/projects">Projects</router-link>
    </nav>
</header>

<!-- Render the component for the corresponding route -->
<router-view></router-view>
```

We also used the router-view component that will render a specific component for each route.
Now we need to do something inside our javascript file to make this work. 

Let's see the steps we need to take:
- Define route components
- Define routes
- Create a Vue router instance
- Create and mounth the Vue root instance.

First we need to define our components that will be used from each route to render the content of the page.
We will create two components, one for the home page and one for the projects page.

To simplify the steps we will keep everything in the same file and refactor later on. 
Let's create first two basic components to see if the router works

```js
// Create Route components
const Home = {template: '<div>My Portfolio</div>'} 
const Projects = {template: '<div> Projects </div>'} 
```
Now let's follow the remaining steps and define the routes, create the vue router instance and create and mount the Vue root instance.
```js

// Define some routes
const routes = [
    {path: '/', component: Home},
    {path: '/projects', component: Projects}
];
// Create the router instance and pass the routes to it
const router = new VueRouter({
routes: routes
});
// Create and mount the root instance.

let app = new Vue({
    router 
}).$mount('#app');

```

That's it. If you visit the homepage you will see two navigation links and the site content will change accordingly.
Let's put all together and start building our portfolio.
From the previous example in the Axios section we requested from the GitHub API all public repositories for a user and rendered name, user avatar and project url onto the page. Let's move some of that logic now inside our application that uses routes.

The main changes that we need to make here are:
- move the HTML markup inside the `template` property of the projects component
- move the `data` properties inside the `data` object of the component
- move the code we wrote in the mounted hook inside our component.

The final code looks something like that:
```js
// Define route components

const Home = {template: '<div>My Portfolio</div>'} 
const Projects = {
    
    template: `<div> 
         <div v-for="project in projects">
            <h2 class="title">{{project.full_name}}</h2>
            
            <div class="author">
                <img width="50px" :src="project.owner.avatar_url" alt="me">
            </div>
            <div class="view">
                <a :href="project.html_url">View</a>
            </div>
        </div>
    </div>`,
    data(){
        return {
            projects: [],
            perPage: 20,
            page: 1
        }
    }, 
    mounted(){
        
         axios
         .get(`https://api.github.com/users/fabiopacifici/repos?per_page=${this.perPage}&page=${this.page}`)
         .then(
            response => {
                //console.log(response);
                this.projects = response.data;
            }
        )
        .catch(error=> {console.log(error);})
    }
} 

// Define some routes
const routes = [
    {path: '/', component: Home},
    {path: '/projects', component: Projects}
];
// Create the router instance and pass the routes to it
const router = new VueRouter({
routes: routes
});
// Create and mount the root instance.

let app = new Vue({
    router 
}).$mount('#app');

```


The HTML file remains the same

```html
<div id='app'>
  <header>
            <nav>
                <router-link to="/">Home</router-link>
                <router-link to="/projects">Projects</router-link>
            </nav>
        </header>

        <router-view></router-view>
</div>

```

Now that we have a base to work with, let's improve it. 
We will use a design prototype I made using Figma and add some functionalities to our portfolio to make it nice.

Until the next time remember to subscribe to the channel, like the video and leave a comment if you have any question. 
Stay safe guys, Bye!